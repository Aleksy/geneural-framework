package aleksy.geneuralframework.neuralnetstructure.exception.abstr;

import aleksy.geneuralframework.common.exception.GeneuralException;

/**
 * Neural network exception
 */
public abstract class NeuralNetworkException extends GeneuralException {
    public NeuralNetworkException(String message) {
        super("neural network -> " + message);
    }
}
