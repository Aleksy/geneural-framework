package aleksy.geneuralframework.neuralnetstructure.exception.abstr;

/**
 * Neuron exception
 */
public abstract class NeuronException extends NeuralNetworkException {
    public NeuronException(String message) {
        super("neuron -> " + message);
    }
}
