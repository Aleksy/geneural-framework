package aleksy.geneuralframework.neuralnetstructure.exception;

import aleksy.geneuralframework.neuralnetstructure.exception.abstr.NeuralLayerException;

/**
 * Incorrect number of inputs in layer exception
 */
public class IncorrectNumberOfInputsInLayerException extends NeuralLayerException {

    public IncorrectNumberOfInputsInLayerException() {
        super("incorrect number of inputs.");
    }

    public IncorrectNumberOfInputsInLayerException(String message) {
        super("incorrect number of inputs. MESSAGE: \"" + message + "\"");
    }
}
