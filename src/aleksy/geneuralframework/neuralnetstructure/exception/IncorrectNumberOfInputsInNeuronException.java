package aleksy.geneuralframework.neuralnetstructure.exception;

import aleksy.geneuralframework.neuralnetstructure.exception.abstr.NeuronException;

/**
 * Incorrect number of inputs in neuron exception
 */
public class IncorrectNumberOfInputsInNeuronException extends NeuronException {

    public IncorrectNumberOfInputsInNeuronException() {
        super("incorrect number of inputs.");
    }

    public IncorrectNumberOfInputsInNeuronException(String message) {
        super("incorrect number of inputs. MESSAGE: \"" + message + "\"");
    }
}
