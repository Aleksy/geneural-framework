package aleksy.geneuralframework.neuralnetstructure.builder;

import aleksy.geneuralframework.neuralnetstructure.enumerate.NeuralNetworkType;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;

/**
 * Endpoint builder of neural network - last step in network building
 */
public class EndpointBuilder {
    private NeuralNetworkInterface network;

    /**
     * Constructor
     * @param network to use in builder
     */
    public EndpointBuilder(NeuralNetworkInterface network) {
        this.network = network;
    }

    /**
     * Creates a new neural network with default description
     * @return neural network ready to use
     */
    public NeuralNetworkInterface create() {
        network.setDescription("neural network created in builder");
        network.setType(NeuralNetworkType.FEEDFORWARD);
        if(network.isBiasPresence())
            network.createBiasWeights();
        return network;
    }

    /**
     * Creates a new neural network with custom description
     * @param description to set in network
     * @return new neural network ready to use
     */
    public NeuralNetworkInterface create(String description) {
        network.setDescription(description);
        network.setType(NeuralNetworkType.FEEDFORWARD);
        if(network.isBiasPresence())
            network.createBiasWeights();
        return network;
    }
}
