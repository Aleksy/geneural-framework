package aleksy.geneuralframework.neuralnetstructure.builder;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.neuralnetstructure.config.StructureConfigurations;
import aleksy.geneuralframework.neuralnetstructure.model.function.AxonFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.CoreFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.WeightFunction;
import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;
import aleksy.geneuralframework.neuralnetstructure.model.layer.impl.GeneuralLayer;
import aleksy.geneuralframework.neuralnetstructure.model.network.AbstractNeuralNetwork;
import aleksy.geneuralframework.neuralnetstructure.model.network.impl.GeneuralNeuralNetwork;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.impl.GeneuralNeuron;
import aleksy.geneuralframework.neuralnetstructure.util.AxonFunctions;
import aleksy.geneuralframework.neuralnetstructure.util.CoreFunctions;
import aleksy.geneuralframework.neuralnetstructure.util.WeightFunctions;

import java.util.ArrayList;
import java.util.List;

/**
 * Neural network builder - starting point in network building
 */
public class GeneuralNetworkBuilder {
    private StructureConfigurations configurations;
    private AbstractNeuralNetwork rawNetwork;
    private AxonFunction axon;
    private CoreFunction core;
    private GeneuralFacadeInterface geneural;

    /**
     * Constructor
     * @param configurations to use in building of basic neural network parameters
     * @param geneural facade
     */
    public GeneuralNetworkBuilder(StructureConfigurations configurations, GeneuralFacadeInterface geneural) {
        this.configurations = configurations;
        rawNetwork = new GeneuralNeuralNetwork();
        axon = AxonFunctions.newInstance(configurations.axonFunctionRecipe);
        axon.setAxonFunctionRecipe(configurations.axonFunctionRecipe);
        core = CoreFunctions.newInstance(configurations.coreFunctionRecipe);
        core.setCoreFunctionRecipe(configurations.coreFunctionRecipe);
        rawNetwork.setBiasPresence(configurations.biasPresence);
        rawNetwork.setDataNormalization(configurations.dataNormalization);
        this.geneural = geneural;
    }

    /**
     * Adds default input layer. In feedforward networks whole input vector is given to all neurons of
     * this layer, so it's no matter how much neurons are placed here. When you builds this layer
     * please don't worry about number of inputs for every neurons - Geneural Frameworks knows what
     * to do.
     * @param numberOfNeurons to set in input layer
     * @param sizeOfInputVector number of coordinates in input vector, and number of inputs of each
     *                          vector in input layer.
     * @return builder
     */
    public NetworkWithInputLayerBuilder addDefaultInputLayer(int numberOfNeurons, int sizeOfInputVector) {
        NeuralLayerInterface layer = createLayer(numberOfNeurons, sizeOfInputVector);
        layer.setInputLayer(true);
        List<NeuralLayerInterface> layers = new ArrayList<>();
        layers.add(layer);
        rawNetwork.setLayers(layers);
        return new NetworkWithInputLayerBuilder(configurations, rawNetwork, core, axon, geneural);
    }

    /**
     * Creates one layer and returns {@link EndpointBuilder}. Each neuron gets whole input vector to
     * his inputs.
     * @param numberOfNeurons to set in input layer
     * @param sizeOfInputVector number of coordinates in input vector, and number of inputs of each
     *                          vector in input layer.
     * @param randomizeWeights randomizing the neural network weights when true
     * @return endpoint builder
     */
    public EndpointBuilder buildSingleLayerNetwork(int numberOfNeurons,
                                                   int sizeOfInputVector, boolean randomizeWeights) {
        NeuralLayerInterface layer = createLayer(numberOfNeurons, sizeOfInputVector);
        layer.setInputLayer(true);
        List<NeuralLayerInterface> layers = new ArrayList<>();
        layers.add(layer);
        rawNetwork.setLayers(layers);
        if(randomizeWeights)
            rawNetwork.randomizeWeights();
        return new EndpointBuilder(rawNetwork);
    }

    private NeuralLayerInterface createLayer(int numberOfNeurons, int amountOfInputsOfEachNeuron) {
        NeuralLayerInterface layer = new GeneuralLayer();
        List<NeuronInterface> neurons = new ArrayList<>();
        for(int i = 0; i < numberOfNeurons; i++) {
            List<WeightFunction> weights = new ArrayList<>();
            for (int j = 0; j < amountOfInputsOfEachNeuron; j++) {
                weights.add(WeightFunctions.newInstance(configurations.weightFunctionRecipe, false));
            }
            neurons.add(new GeneuralNeuron(weights, core, axon, rawNetwork.isBiasPresence()));
        }
        layer.setNeurons(neurons);
        layer.setDescription("default input layer created in builder");
        return layer;
    }
}
