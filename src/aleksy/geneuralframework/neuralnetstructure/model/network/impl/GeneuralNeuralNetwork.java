package aleksy.geneuralframework.neuralnetstructure.model.network.impl;

import aleksy.geneuralframework.neuralnetstructure.model.network.AbstractNeuralNetwork;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;

/**
 * Basic implementation of neural network
 */
public class GeneuralNeuralNetwork extends AbstractNeuralNetwork implements NeuralNetworkInterface {
}
