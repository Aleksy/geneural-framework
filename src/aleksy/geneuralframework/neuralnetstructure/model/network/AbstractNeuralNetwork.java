package aleksy.geneuralframework.neuralnetstructure.model.network;

import aleksy.geneuralframework.common.constants.GeneuralTechnicalConstants;
import aleksy.geneuralframework.neuralnetstructure.enumerate.DataNormalizationType;
import aleksy.geneuralframework.neuralnetstructure.enumerate.NeuralNetworkType;
import aleksy.geneuralframework.neuralnetstructure.enumerate.WeightFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.geneuralframework.neuralnetstructure.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.geneuralframework.neuralnetstructure.model.function.AxonFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.CoreFunction;
import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;
import aleksy.geneuralframework.neuralnetstructure.model.layer.impl.GeneuralLayer;
import aleksy.geneuralframework.neuralnetstructure.model.network.impl.GeneuralNeuralNetwork;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.impl.GeneuralNeuron;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract neural network
 */
public abstract class AbstractNeuralNetwork implements NeuralNetworkInterface, Comparable<NeuralNetworkInterface> {
    private List<NeuralLayerInterface> layers;
    private String description;
    private NeuralNetworkType type;
    private Double rate;
    private boolean biasPresence;
    private DataNormalizationType dataNormalization;

    @Override
    public List<Double> f(List<Double> xs) throws
            IncorrectNumberOfInputsInNeuronException, IncorrectNumberOfInputsInLayerException {
        List<Double> inputs = new ArrayList<>(xs);
        if(dataNormalization != DataNormalizationType.NO_NORMALIZATION) normalizeInputs(inputs);
        if(biasPresence)
               inputs.add(GeneuralTechnicalConstants.BIAS);
        List<Double> internalOuts = new ArrayList<>(inputs);
        for(int i = 0; i < layers.size(); i++) {
            internalOuts = layers.get(i).f(internalOuts);
            if(biasPresence)
                internalOuts.add(GeneuralTechnicalConstants.BIAS);
        }
            if(biasPresence)
        internalOuts.remove(internalOuts.size() - 1);
        return internalOuts;
    }

    @Override
    public int size() {
        int size = 0;
        for(NeuralLayerInterface layer : layers)
            size += layer.size();
        return size;
    }

    @Override
    public int numberOfLayers() {
        return layers.size();
    }

    @Override
    public NeuralLayerInterface getInputLayer() {
        return layers.get(0);
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void setLayers(List<NeuralLayerInterface> layers) {
        this.layers = layers;
    }

    @Override
    public List<NeuralLayerInterface> getLayers() {
        return layers;
    }

    @Override
    public void setType(NeuralNetworkType type) {
        this.type = type;
    }

    @Override
    public NeuralNetworkType getType() {
        return type;
    }

    @Override
    public void randomizeWeights() {
        for(NeuralLayerInterface layer : layers) {
            layer.randomizeWeights();
        }
    }

    @Override
    public CoreFunction getCoreFunction() {
        return getLayers().get(0).getNeurons().get(0).getCoreFunction();
    }

    @Override
    public AxonFunction getAxonFunction() {
        return getLayers().get(0).getNeurons().get(0).getAxonFunction();
    }

    @Override
    public NeuralNetworkInterface hybridize(NeuralNetworkInterface network, double mutationChange) {
        NeuralNetworkInterface childNetwork = new GeneuralNeuralNetwork();
        List<NeuralLayerInterface> childLayers = new ArrayList<>();
        for(int i = 0; i < getLayers().size(); i++) {
            childLayers.add(layers.get(i).hybridize(network.getLayers().get(i), mutationChange));
        }
        childNetwork.setLayers(childLayers);
        childNetwork.setDescription(description);
        childNetwork.setType(type);
        childNetwork.setBiasPresence(isBiasPresence());
        return childNetwork;
    }

    @Override
    public Double getRate() {
        return rate;
    }

    @Override
    public void setRate(Double rate) {
        this.rate = rate;
    }

    @Override
    public int compareTo(NeuralNetworkInterface o) {
        if(o.getRate() > getRate())
            return -1;
        if(o.getRate() < getRate())
            return 1;
        return 0;
    }

    @Override
    public void setBiasPresence(boolean biasPresence) {
        this.biasPresence = biasPresence;
    }

    @Override
    public boolean isBiasPresence() {
        return biasPresence;
    }

    @Override
    public void createBiasWeights() {
        List<NeuronInterface> allNeurons = this.getAllNeurons();
        for(NeuronInterface neuron : allNeurons) {
            neuron.addRandomBiasWeight();
        }
    }

    private List<NeuronInterface> getAllNeurons() {
        List<NeuronInterface> allNeurons = new ArrayList<>();
        for(NeuralLayerInterface layer : layers) {
            allNeurons.addAll(layer.getNeurons());
        }
        return allNeurons;
    }

    @Override
    public WeightFunctionRecipe getWeightFunctionRecipe() {
        return getLayers().get(0).getNeurons().get(0).getWeightFunctionRecipe();
    }

    @Override
    public NeuralNetworkInterface copy() {
        NeuralNetworkInterface network = new GeneuralNeuralNetwork();
        network.setType(this.getType());
        network.setDescription(this.getDescription());
        network.setBiasPresence(this.isBiasPresence());
        network.setDataNormalization(this.getDataNormalization());
        List<NeuralLayerInterface> layers = new ArrayList<>();
        for(NeuralLayerInterface layer : this.getLayers()) {
            NeuralLayerInterface newLayer = new GeneuralLayer();
            List<NeuronInterface> neurons = new ArrayList<>();
            for(NeuronInterface neuron : layer.getNeurons()) {
                neurons.add(new GeneuralNeuron(neuron.cloneWeights(),
                        this.getCoreFunction(), this.getAxonFunction(), this.isBiasPresence()));
            }
            newLayer.setNeurons(neurons);
            newLayer.setInputLayer(false);
            newLayer.setDescription(layer.getDescription());
            layers.add(newLayer);
        }
        network.setLayers(layers);
        network.getLayers().get(0).setInputLayer(true);
        return network;
    }

    @Override
    public NeuralLayerInterface getOutputLayer() {
        return layers.get(layers.size() - 1);
    }

    @Override
    public void setDataNormalization(DataNormalizationType dataNormalization) {
        this.dataNormalization = dataNormalization;
    }

    @Override
    public DataNormalizationType getDataNormalization() {
        return dataNormalization;
    }

    private List<Double> normalizeInputs(List<Double> inputs) {
        if(dataNormalization == DataNormalizationType.SCALING_TO_AVERAGE)
            return scaleToAverage(inputs);
        if(dataNormalization == DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR)
            return normalizeToUnitVector(inputs);
        if(dataNormalization == DataNormalizationType.SCALING_TO_DEVIATION_FROM_AVERAGE)
            return scaleToDeviationFromAverage(inputs);
        if(dataNormalization == DataNormalizationType.SCAlING_TO_DEVIATION_FROM_MIN_VALUE)
            return scaleToDeviationFromMinValue(inputs);
        if(dataNormalization == DataNormalizationType.SCALING_TO_MAX_VALUE)
            return scaleToMaxValue(inputs);
        if(dataNormalization == DataNormalizationType.SCALING_TO_STANDARD_DEVIATION)
            return scaleToStandardDeviation(inputs);
        return inputs;
    }

    private List<Double> scaleToStandardDeviation(List<Double> inputs) {
        List<Double> normalized = new ArrayList<>();
        Double avg = 0.0;
        for(Double x : inputs) {
            avg += x;
        }
        avg /= (double)inputs.size();
        List<Double> process = new ArrayList<>();
        for(Double x : inputs)
            process.add(Math.pow(x - avg, 2));
        Double sd = 0.0;
        for(Double x : process)
            sd += x;
        sd /= Math.sqrt((double)process.size());

        for(Double x : inputs)
            normalized.add((x - avg) / sd);
        return normalized;
    }

    private List<Double> scaleToAverage(List<Double> inputs) {
        List<Double> normalized = new ArrayList<>();
        Double avg = 0.0;
        for(Double x : inputs) {
            avg += x;
        }
        avg /= (double)inputs.size();
        for(Double x : inputs) {
            normalized.add(x / avg);
        }
        return normalized;
    }

    private List<Double> scaleToDeviationFromAverage(List<Double> inputs) {
        List<Double> normalized = new ArrayList<>();
        Double avg = 0.0;
        for(Double x : inputs) {
            avg += x;
        }
        avg /= (double)inputs.size();
        for(Double x : inputs)
            normalized.add((x - avg) / avg);
        return normalized;
    }

    private List<Double> scaleToDeviationFromMinValue(List<Double> inputs) {
        List<Double> normalized = new ArrayList<>();
        Double min = Double.MAX_VALUE;
        Double max = Double.MIN_VALUE;
        for(Double x : inputs) {
            if(x < min)
                min = x;
            if(x > max)
                max = x;
        }
        for(Double x : inputs)
            normalized.add((x - min) / (max - min));
        return normalized;
    }

    private List<Double> scaleToMaxValue(List<Double> inputs) {
        List<Double> normalized = new ArrayList<>();
        Double max = Double.MIN_VALUE;
        for(Double x : inputs) {
            if(x > max)
                max = x;
        }
        for(Double x : inputs)
            normalized.add(x / max);
        return normalized;
    }

    private List<Double> normalizeToUnitVector(List<Double> inputs) {
        List<Double> normalized = new ArrayList<>();
        Double sqrSum = 0.0;
        for(Double x : inputs)
            sqrSum += x * x;
        Double length = Math.sqrt(sqrSum);

        for(Double x : inputs)
            normalized.add(x / length);
        return normalized;
    }
}