package aleksy.geneuralframework.neuralnetstructure.model.network;

import aleksy.geneuralframework.neuralnetstructure.enumerate.DataNormalizationType;
import aleksy.geneuralframework.neuralnetstructure.enumerate.NeuralNetworkType;
import aleksy.geneuralframework.neuralnetstructure.enumerate.WeightFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.geneuralframework.neuralnetstructure.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.geneuralframework.neuralnetstructure.model.function.AxonFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.CoreFunction;
import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;
import aleksy.geneuralframework.teaching.logic.GeneticAlgorithmLogic;

import java.util.List;

/**
 * Neural network interface
 */
public interface NeuralNetworkInterface {
    /**
     * Function calculates values of outputs vector from network
     * @param xs inputs vector
     * @return outputs vector
     * @throws IncorrectNumberOfInputsInLayerException when input vector has wrong size
     * @throws IncorrectNumberOfInputsInNeuronException when input vector has wrong size
     */
    List<Double> f(List<Double> xs) throws IncorrectNumberOfInputsInNeuronException, IncorrectNumberOfInputsInLayerException;

    /**
     * Setter for layers of network
     * @param layers list to set
     */
    void setLayers(List<NeuralLayerInterface> layers);

    /**
     * Getter for list of layers of network
     * @return layers
     */
    List<NeuralLayerInterface> getLayers();

    /**
     * Method returns input layer
     * @return input layer of network
     */
    NeuralLayerInterface getInputLayer();

    /**
     * Functions count all neurons
     * @return number of neurons in network
     */
    int size();

    /**
     * Functions count all layers
     * @return number of layers in network
     */
    int numberOfLayers();

    /**
     * Setter for string description of network
     * @param description to set
     */
    void setDescription(String description);

    /**
     * Getter for string description of network
     * @return description of network
     */
    String getDescription();

    /**
     * Getter for network type
     * @return type
     */
    NeuralNetworkType getType();

    /**
     * Setter for network type
     * @param type to set
     */
    void setType(NeuralNetworkType type);

    /**
     * Clones itself to new instance of network
     * @return cloned instance
     */
    NeuralNetworkInterface copy();

    /**
     * Randomizes weights functions
     */
    void randomizeWeights();

    /**
     * Getter for core function instance
     * @return core function instance used in all neurons of network
     */
    CoreFunction getCoreFunction();

    /**
     * Getter for axon function instance
     * @return axon function instance used in all neurons of network
     */
    AxonFunction getAxonFunction();

    /**
     * Hybridizes network with another network given in parameter. Method used in genetic algorithm.
     * @param network to hybridize
     * @param mutationChange of single factor in weight function of neuron
     * @return new child of hybridized networks
     */
    NeuralNetworkInterface hybridize(NeuralNetworkInterface network, double mutationChange);

    /**
     * Setter for neural network rate. (See also {@link GeneticAlgorithmLogic})
     * @param rate to set
     */
    void setRate(Double rate);

    /**
     * Getter for neural network rate.  (See also {@link GeneticAlgorithmLogic})
     * @return rate of neural network
     */
    Double getRate();

    /**
     * Getter for bias presence boolean value
     * @return bias presence
     */
    boolean isBiasPresence();

    /**
     * Setter for bias presence boolean value
     * @param biasPresence to set
     */
    void setBiasPresence(boolean biasPresence);

    /**
     * Method creates one additional weight for each neuron of network. This method is used during building of
     * neural network when bias presence value is true.
     */
    void createBiasWeights();

    /**
     * Getter for weight function recipe
     * @return weight function recipe
     */
    WeightFunctionRecipe getWeightFunctionRecipe();

    /**
     * Getter for last layer in network
     * @return output layer
     */
    NeuralLayerInterface getOutputLayer();

    /**
     * Setter for data normalization
     * @param dataNormalization to set
     */
    void setDataNormalization(DataNormalizationType dataNormalization);

    /**
     * Getter for data normalization
     * @return data normalization
     */
    DataNormalizationType getDataNormalization();
}
