package aleksy.geneuralframework.neuralnetstructure.model.layer;

import aleksy.geneuralframework.neuralnetstructure.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.geneuralframework.neuralnetstructure.model.layer.impl.GeneuralLayer;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract neural network layer
 */
public abstract class AbstractNeuralLayer implements NeuralLayerInterface {
    protected List<NeuronInterface> neurons;
    protected String description;
    protected boolean isInputLayer;

    @Override
    public void setNeurons(List<NeuronInterface> neurons) {
        this.neurons = neurons;
    }

    @Override
    public List<NeuronInterface> getNeurons() {
        return neurons;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<Double> f(List<Double> xs) throws IncorrectNumberOfInputsInNeuronException {
        List<Double> out = new ArrayList<>();
        for(NeuronInterface neuron : neurons) {
            out.add(neuron.f(xs));
        }
        return out;
    }

    @Override
    public void setInputLayer(boolean inputLayer) {
        isInputLayer = inputLayer;
    }

    @Override
    public boolean isInputLayer() {
        return isInputLayer;
    }

    @Override
    public int numberOfInputs() {
        int inputs = 0;
        for(NeuronInterface neuron : neurons) {
            inputs += neuron.size();
        }
        return inputs;
    }

    @Override
    public int size() {
        return neurons.size();
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("<LAYER>\n");
        for(NeuronInterface n : neurons) {
            s.append(n.toString()).append("\n");
        }
        s.append("</LAYER>");
        return s.toString();
    }

    @Override
    public void randomizeWeights() {
        for(NeuronInterface neuron : neurons) {
            neuron.randomizeWeights();
        }
    }

    @Override
    public NeuralLayerInterface hybridize(NeuralLayerInterface layer, double mutationChance) {
        NeuralLayerInterface childLayer = new GeneuralLayer();
        List<NeuronInterface> childNeurons = new ArrayList<>();
        for(int i = 0; i < layer.size(); i++) {
            childNeurons.add(layer.getNeurons().get(i).hybridize(getNeurons().get(i), mutationChance));
        }
        childLayer.setNeurons(childNeurons);
        childLayer.setInputLayer(isInputLayer);
        childLayer.setDescription(description);
        return childLayer;
    }
}
