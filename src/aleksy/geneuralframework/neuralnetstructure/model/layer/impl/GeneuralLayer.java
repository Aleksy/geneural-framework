package aleksy.geneuralframework.neuralnetstructure.model.layer.impl;

import aleksy.geneuralframework.neuralnetstructure.model.layer.AbstractNeuralLayer;
import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;

/**
 * Basic implementation of neural network layer
 */
public class GeneuralLayer extends AbstractNeuralLayer implements NeuralLayerInterface {
}
