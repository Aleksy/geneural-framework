package aleksy.geneuralframework.neuralnetstructure.model.layer;

import aleksy.geneuralframework.neuralnetstructure.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.geneuralframework.neuralnetstructure.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;

import java.util.List;

/**
 * Neural layer interface
 */
public interface NeuralLayerInterface {
    /**
     * Function calculates a neural layer outputs of each neuron from previous layer
     * @param xs vector of inputs from previous layer (or from external input)
     * @return vector of outputs
     * @throws IncorrectNumberOfInputsInLayerException when number of inputs is not equal to
     * number of neuron inputs in layer
     * @throws IncorrectNumberOfInputsInNeuronException when number of inputs is not equal to
     * number of neuron inputs in layer
     */
    List<Double> f(List<Double> xs) throws IncorrectNumberOfInputsInLayerException, IncorrectNumberOfInputsInNeuronException;

    /**
     * Setter for description of layer
     * @param description to set
     */
    void setDescription(String description);

    /**
     * Getter for description of layer
     * @return layer description
     */
    String getDescription();

    /**
     * Setter for neurons list
     * @param neurons list to set
     */
    void setNeurons(List<NeuronInterface> neurons);

    /**
     * Getter for neurons list
     * @return list of neurons
     */
    List<NeuronInterface> getNeurons();

    /**
     * Getter for isInputLayer
     * @return true if layer is input layer
     */
    boolean isInputLayer();

    /**
     * Setter for isInputLayer
     * @param isInputLayer to set
     */
    void setInputLayer(boolean isInputLayer);

    /**
     * Function counts all neurons in layer
     * @return number of neurons in layer
     */
    int size();

    /**
     * Function counts all inputs of all neurons in layer
     * @return number of inputs in layer
     */
    int numberOfInputs();

    /**
     * Randomizes weights functions
     */
    void randomizeWeights();

    /**
     * Hybridize layer with another layer given in parameter
     * @param layer to hybridize
     * @param mutationChance of single weight function factor
     * @return new child of two layers
     */
    NeuralLayerInterface hybridize(NeuralLayerInterface layer, double mutationChance);
}
