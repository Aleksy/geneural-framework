package aleksy.geneuralframework.neuralnetstructure.model.function;

import aleksy.geneuralframework.neuralnetstructure.enumerate.CoreFunctionRecipe;

/**
 * Abstract core function
 */
public abstract class AbstractCoreFunction implements CoreFunction {
    protected CoreFunctionRecipe coreFunctionRecipe;

    @Override
    public CoreFunctionRecipe getCoreFunctionRecipe() {
        return coreFunctionRecipe;
    }

    @Override
    public void setCoreFunctionRecipe(CoreFunctionRecipe coreFunctionRecipe) {
        this.coreFunctionRecipe = coreFunctionRecipe;
    }
}
