package aleksy.geneuralframework.neuralnetstructure.model.function;

import aleksy.geneuralframework.neuralnetstructure.enumerate.WeightFunctionRecipe;

import java.util.ArrayList;
import java.util.List;

/**
 * Abstract weight function
 */
public abstract class AbstractWeightFunction implements WeightFunction {

    protected List<Double> factors;
    protected WeightFunctionRecipe weightFunctionRecipe;

    public AbstractWeightFunction() {
        this.factors = new ArrayList<>();
        initFactors();
    }

    public AbstractWeightFunction(WeightFunction weightFunction) {
        this.factors = weightFunction.getFactors();
    }

    public void setFactors(List<Double> factors) {
        this.factors = factors;
    }

    public List<Double> getFactors() {
        return factors;
    }

    @Override
    public void setWeightFunctionRecipe(WeightFunctionRecipe weightFunctionRecipe) {
        this.weightFunctionRecipe = weightFunctionRecipe;
    }

    @Override
    public WeightFunctionRecipe getWeightFunctionRecipe() {
        return weightFunctionRecipe;
    }
}
