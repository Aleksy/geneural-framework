package aleksy.geneuralframework.neuralnetstructure.model.function;

import aleksy.geneuralframework.neuralnetstructure.enumerate.CoreFunctionRecipe;

import java.util.List;

/**
 * Core function interface
 */
public interface CoreFunction extends Function {
    /**
     * Function in specific way changes vector of outputs of weight functions to one internal output for axon function
     * @param xs vector of outputs from weight functions
     * @return internal output for axon function in specific data type
     */
    Double f(List<Double> xs);

    /**
     * Getter for core function recipe
     * @return core function recipe
     */
    CoreFunctionRecipe getCoreFunctionRecipe();

    /**
     * Setter for core function recipe
     * @param coreFunctionRecipe to set
     */
    void setCoreFunctionRecipe(CoreFunctionRecipe coreFunctionRecipe);
}
