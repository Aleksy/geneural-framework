package aleksy.geneuralframework.neuralnetstructure.model.function;

import aleksy.geneuralframework.neuralnetstructure.enumerate.AxonFunctionRecipe;

/**
 * Abstract axon function
 */
public abstract class AbstractAxonFunction implements AxonFunction {
    protected AxonFunctionRecipe axonFunctionRecipe;

    @Override
    public AxonFunctionRecipe getAxonFunctionRecipe() {
        return axonFunctionRecipe;
    }

    @Override
    public void setAxonFunctionRecipe(AxonFunctionRecipe axonFunctionRecipe) {
        this.axonFunctionRecipe = axonFunctionRecipe;
    }
}
