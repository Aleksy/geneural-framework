package aleksy.geneuralframework.neuralnetstructure.util;

import aleksy.geneuralframework.neuralnetstructure.enumerate.WeightFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.model.function.AbstractWeightFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.WeightFunction;

import java.util.Random;

public class WeightFunctions {
    private static class DEFAULT_LINEAR extends AbstractWeightFunction {
        public DEFAULT_LINEAR(boolean randomizeFactors) {
            super();
            weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
            if(randomizeFactors) {
                for(int i = 0 ; i < factors.size(); i++)
                    factors.set(i, 1 - new Random().nextDouble() * 2);
            }
        }

        @Override
        public Double f(Double x) {
            return x * factors.get(0);
        }

        @Override
        public void initFactors() {
            factors.add(1.0);
        }

        @Override
        public String toString() {
            return factors.get(0).toString();
        }
    }

    /**
     * Method creates the new instance of weight function to set to each neuron
     * @param weightFunctionRecipe says which kind of weight function method creates
     * @param randomizeFactors new instance has randomized factors when true
     * @return new instance of weight function
     */
    public static WeightFunction newInstance(WeightFunctionRecipe weightFunctionRecipe, boolean randomizeFactors) {
        if (weightFunctionRecipe == WeightFunctionRecipe.DEFAULT_LINEAR)
            return new DEFAULT_LINEAR(randomizeFactors);
        return null;
    }
}
