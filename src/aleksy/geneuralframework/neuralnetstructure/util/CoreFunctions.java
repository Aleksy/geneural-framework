package aleksy.geneuralframework.neuralnetstructure.util;

import aleksy.geneuralframework.neuralnetstructure.enumerate.CoreFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.model.function.AbstractCoreFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.CoreFunction;

import java.util.List;

/**
 * Util class for core functions
 */
public class CoreFunctions {
    private static class DEFAULT_ADDER extends AbstractCoreFunction {
        @Override
        public Double f(List<Double> xs) {
            Double sum = 0.0;
            for(Double x : xs)
                sum += x;
            return sum;
        }
    }

    /**
     * Method creates the new instance of core function to set to neural network
     * @param coreFunctionRecipe says which kind of core function method creates
     * @return new instance of core function
     */
    public static CoreFunction newInstance(CoreFunctionRecipe coreFunctionRecipe) {
        CoreFunction core = null;
        if(coreFunctionRecipe == CoreFunctionRecipe.DEFAULT_ADDER)
            core = new DEFAULT_ADDER();
        if(core != null)
            core.setCoreFunctionRecipe(coreFunctionRecipe);
        return core;
    }
}
