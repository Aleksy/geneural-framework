package aleksy.geneuralframework.neuralnetstructure.enumerate;

/**
 * Neural network types
 */
public enum NeuralNetworkType {
    /**
     * Single- or multilayer feedforward type
     */
    FEEDFORWARD
}
