package aleksy.geneuralframework.neuralnetstructure.enumerate;

/**
 * Weight functions recipes
 */
public enum WeightFunctionRecipe {
    /**
     * Default linear function with one factor
     */
    DEFAULT_LINEAR
}
