package aleksy.geneuralframework.neuralnetstructure.enumerate;

/**
 * Axon functions recipes
 */
public enum AxonFunctionRecipe {
    /**
     * Hyperbolic tangent
     */
    TANH,
    /**
     * Default linear function
     */
    LINEAR,
    /**
     * Threshold function
     */
    THRESHOLD,
    /**
     * Hyperbolic sinus
     */
    SINH
}
