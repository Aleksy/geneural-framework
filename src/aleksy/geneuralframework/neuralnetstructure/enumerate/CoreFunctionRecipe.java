package aleksy.geneuralframework.neuralnetstructure.enumerate;

/**
 * Core functions recipes
 */
public enum CoreFunctionRecipe {
    /**
     * Default adding function
     */
    DEFAULT_ADDER
}
