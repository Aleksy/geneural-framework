package aleksy.geneuralframework.teaching.config;

/**
 * Genetic algorithm configurations class
 */
public class GeneticAlgorithmConfigurations {
    /**
     * Amount of networks in single population
     */
    public int amountOfNetworks;
    /**
     * Chance of mutation of single weight in single neuron
     */
    public double mutationChance;
    /**
     * Amount of generations
     */
    public int generations;
    /**
     * Percentage amount of hybridized neural networks. This value defines how many networks from
     * previous population will be used to create new population. For example when percentage value
     * of hybridized networks equals 0.3 and population has 1000 networks, it means, that 300 best
     * networks from this population will be used to create next population
     */
    public double percentageAmountOfHybridizedNetworks;
    /**
     * Expected amount of neural networks in output list
     */
    public int expectedAmountOfNetworksOnOutput;
}
