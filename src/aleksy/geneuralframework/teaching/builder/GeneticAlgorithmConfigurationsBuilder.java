package aleksy.geneuralframework.teaching.builder;

import aleksy.geneuralframework.teaching.config.GeneticAlgorithmConfigurations;

/**
 * Genetic algorithm configurations builder
 */
public class GeneticAlgorithmConfigurationsBuilder {
    private GeneticAlgorithmConfigurations gc;

    /**
     * Constructor
     */
    public GeneticAlgorithmConfigurationsBuilder() {
        gc = new GeneticAlgorithmConfigurations();
    }

    /**
     * Setter for amount of networks in population
     * @param amountOfNetworks to set
     * @return builder
     */
    public  GeneticAlgorithmConfigurationsBuilder setAmountOfNetworksInPopulation(int amountOfNetworks) {
        gc.amountOfNetworks = amountOfNetworks;
        return this;
    }

    /**
     * Setter for mutation chance in genetic algorithm (usually mutation chance is 0.01)
     * @param mutationChance to set
     * @return builder
     */
    public GeneticAlgorithmConfigurationsBuilder setMutationChance(Double mutationChance) {
        gc.mutationChance = mutationChance;
        return this;
    }

    /**
     * Setter for amount of generations
     * @param amountOfGenerations to set
     * @return builder
     */
    public GeneticAlgorithmConfigurationsBuilder setAmountOfGenerations(int amountOfGenerations) {
        gc.generations = amountOfGenerations;
        return this;
    }

    /**
     * Setter for percentage value of hybridized network. This value defines how many networks from
     * previous population will be used to create new population. For example when percentage value
     * of hybridized networks equals 0.3 and population has 1000 networks, it means, that 300 best
     * networks from this population will be used to create next population
     * @param percentageAmountOfHybridizedNetworks to set
     * @return builder
     */
    public GeneticAlgorithmConfigurationsBuilder setPercentageAmountOfHybridizedNetworks(
            Double percentageAmountOfHybridizedNetworks) {
        gc.percentageAmountOfHybridizedNetworks = percentageAmountOfHybridizedNetworks;
        return this;
    }

    /**
     * Setter for expected amount of networks on output of genetic algorithm
     * @param expectedAmountOfNetworksOnOutput to set
     * @return builder
     */
    public GeneticAlgorithmConfigurationsBuilder setExpectedAmountOfNetworksOnOutput(
            int expectedAmountOfNetworksOnOutput) {
        gc.expectedAmountOfNetworksOnOutput = expectedAmountOfNetworksOnOutput;
        return this;
    }

    /**
     * Creation method
     * @return new instance of {@link GeneticAlgorithmConfigurations}
     */
    public GeneticAlgorithmConfigurations create() {
        return gc;
    }
}

