package aleksy.geneuralframework.teaching.logic;

import aleksy.geneuralframework.common.constants.GeneuralTechnicalConstants;
import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.logic.Loggable;
import aleksy.geneuralframework.neuralnetstructure.enumerate.WeightFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.exception.abstr.NeuralNetworkException;
import aleksy.geneuralframework.neuralnetstructure.model.function.WeightFunction;
import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;
import aleksy.geneuralframework.teaching.config.DeepLearningConfigurations;
import aleksy.geneuralframework.teaching.exception.DeepLearningConfigurationsException;
import aleksy.geneuralframework.teachingdata.model.TeachingData;
import aleksy.geneuralframework.teachingdata.model.TeachingDataPackage;

import java.time.Duration;
import java.time.LocalTime;
import java.util.List;
import java.util.Random;

/**
 * Deep learning class
 */
public class DeepLearningLogic implements Loggable {

    private GeneuralFacadeInterface geneural;

    /**
     * Constructor
     * @param geneural facade
     */
    public DeepLearningLogic(GeneuralFacadeInterface geneural) {
        this.geneural = geneural;
    }
    /**
     * Starting method for deep learning algorithm.
     * @param teachingData data with packages to teach network
     * @param network to teach
     * @param deepLearningConfigurations configurations of deep learning parameters
     * @return neural network after teaching process
     * @throws NeuralNetworkException when some problem with internal structure of neural network exists (e. g.
     *                                when input vector size is not correct)
     * @throws DeepLearningConfigurationsException when configurations are wrong
     */
    public NeuralNetworkInterface start(TeachingData teachingData, NeuralNetworkInterface network,
                                        DeepLearningConfigurations deepLearningConfigurations)
            throws NeuralNetworkException, DeepLearningConfigurationsException {
        geneural.log("Deep learning starting.", getSenderName());
        geneural.log("Checking the configurations correctness.", getSenderName());
        configValidation(deepLearningConfigurations);
        LocalTime startTime = LocalTime.now();
        Random r = new Random();
        Double minDefect = Double.MAX_VALUE;
        for(int iteration = 0; iteration < deepLearningConfigurations.iterations; iteration++) {
            TeachingDataPackage tp = teachingData.getDataPackages()
                    .get(r.nextInt(teachingData.getDataPackages().size()));
            List<Double> expectedOuts = tp.expectedOuts;
            List<Double> outs = network.f(tp.ins);

            Double distance = calculateDistanceBetween(expectedOuts, outs);
            if(distance < minDefect)
                minDefect = distance;
            setDefectsToNeurons(expectedOuts, outs, network);
            correctWeightsOfNeurons(tp, network, deepLearningConfigurations);
            if(iteration % 1000 == 0) {
                geneural.log("Itearion " + iteration + "/"
                        + deepLearningConfigurations.iterations + " : min defect value: " + minDefect, getSenderName());
            }
        }
        geneural.log("Finishing. Returning a neural network.", getSenderName());
        LocalTime endTime = LocalTime.now();

        Duration duration = Duration.between(startTime, endTime);

        geneural.log("Time of work: " + duration.getNano() / 1000000 + " ms", getSenderName());
        return network;
    }

    private void correctWeightsOfNeurons(TeachingDataPackage tp, NeuralNetworkInterface network,
                                         DeepLearningConfigurations deepLearningConfigurations) {
        NeuralLayerInterface inputLayer = network.getInputLayer();
        for(int i = 0; i < inputLayer.size(); i++) {
            NeuronInterface inputNeuron = inputLayer.getNeurons().get(i);
            for(int j = 0; j < inputNeuron.getWeights().size(); j++) {
                WeightFunction weight = inputNeuron.getWeights().get(j);
                Double internalInput = j == tp.ins.size() ? GeneuralTechnicalConstants.BIAS : tp.ins.get(j);
                if(inputNeuron.getWeightFunctionRecipe() == WeightFunctionRecipe.DEFAULT_LINEAR) {
                    correctLinearWeight(weight, deepLearningConfigurations, inputNeuron, network, internalInput);
                }
            }
        }

        for(NeuralLayerInterface layer : network.getLayers()) {
            if(!layer.isInputLayer())
                for(NeuronInterface neuron : layer.getNeurons()) {
                    for(int i = 0; i < neuron.getWeights().size(); i++) {
                        WeightFunction weight = neuron.getWeights().get(i);
                        if(neuron.getWeightFunctionRecipe() == WeightFunctionRecipe.DEFAULT_LINEAR) {
                            correctLinearWeight(weight, deepLearningConfigurations, neuron,
                                    network, neuron.getActualInputs().get(i));
                        }
                    }
                }
        }
    }

    private void correctLinearWeight(WeightFunction weight, DeepLearningConfigurations deepLearningConfigurations,
                                     NeuronInterface neuron, NeuralNetworkInterface network, Double internalInput) {
        Double factor = weight.getFactors().get(0);
        factor = factor + deepLearningConfigurations.learningRate
                * internalInput
                * neuron.getDefect()
                * network.getAxonFunction().derivative(neuron.getActualOutputFromCore());
        weight.getFactors().set(0, factor);
    }

    private void setDefectsToNeurons(List<Double> expectedOuts, List<Double> outs,
                                     NeuralNetworkInterface network) {
        NeuralLayerInterface outputLayer = network.getOutputLayer();
        for(int i = 0; i < outs.size(); i++) {
            outputLayer.getNeurons().get(i).setDefect(expectedOuts.get(i) - outs.get(i));
        }

        NeuralLayerInterface nextLayer = outputLayer;
        for(int i = network.getLayers().size() - 2; i >= 0; i--) {
            NeuralLayerInterface hiddenLayer = network.getLayers().get(i);
            for(int j = 0; j < hiddenLayer.getNeurons().size(); j++) {
                NeuronInterface neuron = hiddenLayer.getNeurons().get(j);
                Double defect = 0.0;
                for(NeuronInterface nextNeuron : nextLayer.getNeurons()) {
                    if(network.getWeightFunctionRecipe() == WeightFunctionRecipe.DEFAULT_LINEAR)
                        defect += nextNeuron.getDefect() * nextNeuron.getWeights().get(j).getFactors().get(0);
                }
                neuron.setDefect(defect);
            }
            nextLayer = hiddenLayer;
        }
    }

    private void configValidation(DeepLearningConfigurations deepLearningConfigurations) throws DeepLearningConfigurationsException {
        if(deepLearningConfigurations.learningRate < 0 || deepLearningConfigurations.learningRate > 1)
            throw new DeepLearningConfigurationsException("Learning rate should be from interval 0.0 - 1.0");
        if(deepLearningConfigurations.iterations < 1)
            throw new DeepLearningConfigurationsException("Amount of iterations should be bigger than 0.");
    }

    private Double calculateDistanceBetween(List<Double> expectedOuts, List<Double> outs) {
        Double d = 0.0;
        for(int i = 0; i < outs.size(); i++) {
            d += Math.pow(expectedOuts.get(i) - outs.get(i), 2);
        }
        return Math.sqrt(d);
    }

    @Override
    public String getSenderName() {
        return "DEEP-LRN";
    }
}
