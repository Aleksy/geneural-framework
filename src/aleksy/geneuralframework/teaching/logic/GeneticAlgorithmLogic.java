package aleksy.geneuralframework.teaching.logic;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.logic.Loggable;
import aleksy.geneuralframework.neuralnetstructure.exception.abstr.NeuralNetworkException;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.teaching.config.GeneticAlgorithmConfigurations;
import aleksy.geneuralframework.teaching.exception.GeneticAlgorithmConfigurationsException;
import aleksy.geneuralframework.teaching.exception.GeneticAlgorithmException;
import aleksy.geneuralframework.teachingdata.model.TeachingData;
import aleksy.geneuralframework.teachingdata.model.TeachingDataPackage;

import java.time.Duration;
import java.time.LocalTime;
import java.util.*;

/**
 * Genetic algorithm class
 */
public class GeneticAlgorithmLogic implements Loggable {
    private Double bestAvgDistance;
    private GeneuralFacadeInterface geneural;

    /**
     * Constructor
     * @param geneural facade
     */
    public GeneticAlgorithmLogic(GeneuralFacadeInterface geneural) {
        this.geneural = geneural;
    }

    /**
     * Start method for genetic algorithm.
     * @param teachingData with packages to analyze distances and create rate value for network
     * @param neuralNetwork basic neural network
     * @param configurations with genetic algorithm parameters
     * @return list of neural networks after evolving process
     * @throws GeneticAlgorithmException when there is problem with genetic algorithm configurations
     */
    public List<NeuralNetworkInterface> start(TeachingData teachingData, NeuralNetworkInterface neuralNetwork,
                             GeneticAlgorithmConfigurations configurations) throws GeneticAlgorithmException {
        geneural.log("Genetic algorithm starting.", getSenderName());
        geneural.log("Checking the configurations correctness.", getSenderName());
        configValidation(configurations);
        LocalTime startTime = LocalTime.now();

        bestAvgDistance = Double.MAX_VALUE;
        List<NeuralNetworkInterface> population = new ArrayList<>();
        createFirstPopulation(configurations, neuralNetwork, population);
        geneural.log("First generation of " + population.size() + " networks created.", getSenderName());

        Queue<NeuralNetworkInterface> sortedNetworks = algorithmCore(configurations, population, teachingData);
        List<NeuralNetworkInterface> networksToReturn = new ArrayList<>();
        geneural.log("Finishing. Returning " + configurations.expectedAmountOfNetworksOnOutput
                + " neural networks.", getSenderName());
        for(int i = 0; i < configurations.expectedAmountOfNetworksOnOutput; i++) {
            networksToReturn.add(sortedNetworks.poll());
        }

        LocalTime endTime = LocalTime.now();

        Duration duration = Duration.between(startTime, endTime);

        geneural.log("Time of work: " + duration.getNano() / 1000000 + " ms", getSenderName());

        return networksToReturn;
    }

    private Queue<NeuralNetworkInterface> algorithmCore(GeneticAlgorithmConfigurations configurations,
                               List<NeuralNetworkInterface> population, TeachingData teachingData) {
        Queue<NeuralNetworkInterface> sortedNetworks = rateGeneration(population, teachingData);
        Random rand = new Random();
        int generation = 0;
        while(generation++ <= configurations.generations) {
            List<NeuralNetworkInterface> goodNetworks = new ArrayList<>();
            int amountOfGoodSolutions = (int)(configurations.percentageAmountOfHybridizedNetworks * configurations.amountOfNetworks);
            for(int i = 0; i < amountOfGoodSolutions; i++) {
                goodNetworks.add(sortedNetworks.poll());
            }
            population = new ArrayList<>();
            for(int i = 0; i < configurations.amountOfNetworks; i++) {
                population.add(goodNetworks.get(rand.nextInt(amountOfGoodSolutions))
                        .hybridize(goodNetworks.get(rand.nextInt(amountOfGoodSolutions)), configurations.mutationChance));
            }
            sortedNetworks = rateGeneration(population, teachingData);
            if(generation % 10 == 0)
                geneural.log("Generation " + generation
                        + "/" + configurations.generations + ". The smallest average distance: "
                        + bestAvgDistance, getSenderName());
        }
        return sortedNetworks;
    }

    private void createFirstPopulation(GeneticAlgorithmConfigurations configurations,
                                       NeuralNetworkInterface neuralNetwork, List<NeuralNetworkInterface> population) {
        for(int i = 0; i < configurations.amountOfNetworks; i++) {
            NeuralNetworkInterface n = neuralNetwork.copy();
            n.randomizeWeights();
            population.add(n);
        }
    }

    private void configValidation(GeneticAlgorithmConfigurations configurations) throws GeneticAlgorithmConfigurationsException {
        if(configurations.mutationChance > 1 || configurations.mutationChance < 0)
            throw new GeneticAlgorithmConfigurationsException("Mutation chance should be from interval 0.0 - 1.0");
        if(configurations.percentageAmountOfHybridizedNetworks > 1 || configurations.percentageAmountOfHybridizedNetworks < 0)
            throw new GeneticAlgorithmConfigurationsException("Percentage amount of  hybridized networks be from interval 0.0 - 1.0");
        if(configurations.generations < 1)
            throw new GeneticAlgorithmConfigurationsException("Generations number should be bigger than 0");
        if(configurations.expectedAmountOfNetworksOnOutput > configurations.amountOfNetworks
                || configurations.expectedAmountOfNetworksOnOutput < 1)
            throw new GeneticAlgorithmConfigurationsException("Amount of networks on output should be less than/equal amount of networks on input");
        geneural.log("Configurations are correct, starting an algorithm with:", getSenderName());
        geneural.log("    :mutation chance: " + configurations.mutationChance, getSenderName());
        geneural.log("    :percentage amount of good networks to hybridize: "
                + configurations.percentageAmountOfHybridizedNetworks, getSenderName());
        geneural.log("    :amount of generations: " + configurations.generations, getSenderName());
    }

    private Queue<NeuralNetworkInterface> rateGeneration(List<NeuralNetworkInterface> population, TeachingData teachingData) {
        Queue<NeuralNetworkInterface> sortedNetworks = new PriorityQueue<>();
        for(int i = 0; i < population.size(); i++) {
            NeuralNetworkInterface net = population.get(i);
            Double avgDistance = 0.0;
            for(TeachingDataPackage tp : teachingData.getDataPackages()) {
                try {
                    tp.actualOuts = net.f(tp.ins);
                } catch (NeuralNetworkException e) {
                    e.printStackTrace();
                }
                Double distance = 0.0;
                for(int j = 0; j < tp.actualOuts.size(); j++)
                    distance += Math.pow(tp.expectedOuts.get(j) - tp.actualOuts.get(j), 2);
                distance = Math.sqrt(distance);
                avgDistance += distance;
            }
            avgDistance /= teachingData.getDataPackages().size();
            net.setRate(avgDistance);
            if(avgDistance < bestAvgDistance)
                bestAvgDistance = avgDistance;
            sortedNetworks.add(net);
        }
        return sortedNetworks;
    }

    @Override
    public String getSenderName() {
        return "GENETIC";
    }
}
