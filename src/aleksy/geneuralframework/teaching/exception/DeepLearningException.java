package aleksy.geneuralframework.teaching.exception;

import aleksy.geneuralframework.common.exception.GeneuralException;

public class DeepLearningException extends GeneuralException {
    public DeepLearningException() {
        super();
    }

    public DeepLearningException(String message) {
        super(message);
    }
}
