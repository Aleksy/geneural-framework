package aleksy.geneuralframework.teaching.exception;

import aleksy.geneuralframework.common.exception.GeneuralException;

/**
 * Genetic algorithm exception
 */
public class GeneticAlgorithmException extends GeneuralException {
    public GeneticAlgorithmException() {
        super();
    }

    public GeneticAlgorithmException(String message) {
        super(message);
    }
}
