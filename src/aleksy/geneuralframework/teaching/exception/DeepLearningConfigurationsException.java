package aleksy.geneuralframework.teaching.exception;

public class DeepLearningConfigurationsException extends DeepLearningException {
    public DeepLearningConfigurationsException() {
        super("Deep learning configurations error.");
    }

    public DeepLearningConfigurationsException(String message) {
        super("Deep learning configurations error. " + message);
    }
}
