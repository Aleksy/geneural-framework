package aleksy.geneuralframework.teaching.common.impl;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.neuralnetstructure.exception.abstr.NeuralNetworkException;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.teaching.common.api.TeachingFacadeInterface;
import aleksy.geneuralframework.teaching.config.DeepLearningConfigurations;
import aleksy.geneuralframework.teaching.config.GeneticAlgorithmConfigurations;
import aleksy.geneuralframework.teaching.exception.DeepLearningConfigurationsException;
import aleksy.geneuralframework.teaching.exception.GeneticAlgorithmException;
import aleksy.geneuralframework.teaching.logic.DeepLearningLogic;
import aleksy.geneuralframework.teaching.logic.GeneticAlgorithmLogic;
import aleksy.geneuralframework.teachingdata.model.TeachingData;

import java.util.List;

/**
 * Implementation of {@link TeachingFacadeInterface}
 */
public class Teaching implements TeachingFacadeInterface {
    private GeneuralFacadeInterface geneural;

    public Teaching(GeneuralFacadeInterface geneural) {
        this.geneural = geneural;
    }

    @Override
    public List<NeuralNetworkInterface> geneticAlgorithm(TeachingData teachingData, NeuralNetworkInterface network, GeneticAlgorithmConfigurations geneticAlgorithmConfigurations)
    throws GeneticAlgorithmException {
        return new GeneticAlgorithmLogic(geneural).start(teachingData, network, geneticAlgorithmConfigurations);
    }

    @Override
    public NeuralNetworkInterface deepLearning(TeachingData teachingData, NeuralNetworkInterface network,
                                               DeepLearningConfigurations deepLearningConfigurations)
            throws NeuralNetworkException, DeepLearningConfigurationsException {
        return new DeepLearningLogic(geneural).start(teachingData, network, deepLearningConfigurations);
    }
}
