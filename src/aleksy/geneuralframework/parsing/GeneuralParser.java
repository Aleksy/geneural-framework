package aleksy.geneuralframework.parsing;

import aleksy.geneuralframework.common.logic.Loggable;
import aleksy.geneuralframework.common.util.GeneuralLogger;
import aleksy.geneuralframework.neuralnetstructure.enumerate.*;
import aleksy.geneuralframework.neuralnetstructure.model.function.AxonFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.CoreFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.WeightFunction;
import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;
import aleksy.geneuralframework.neuralnetstructure.model.layer.impl.GeneuralLayer;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.neuralnetstructure.model.network.impl.GeneuralNeuralNetwork;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.impl.GeneuralNeuron;
import aleksy.geneuralframework.neuralnetstructure.util.AxonFunctions;
import aleksy.geneuralframework.neuralnetstructure.util.CoreFunctions;
import aleksy.geneuralframework.neuralnetstructure.util.WeightFunctions;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Geneural standard parser for neural networks
 */
public class GeneuralParser implements Loggable {
    /**
     * Saves a neural network to xml file
     * @param network to save
     * @param filepath where new xml file should be save
     * @throws IOException if is problem with file
     */
    public void save(NeuralNetworkInterface network, String filepath) throws IOException {
        if(!filepath.contains(".xml"))
            filepath += ".xml";
        File file = new File(filepath);
        if(!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
        }
        PrintWriter writer = new PrintWriter(file);
        GeneuralLogger.log("Saving neural network to .xml file.", getSenderName());
        writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>");
        writer.print("    <neuralNetwork ");
        writer.println("description=\"" + network.getDescription() + "\" ");
        writer.println("                   biasPresence=\"" + network.isBiasPresence() + "\" ");
        writer.println("                   dataNormalization=\"" + network.getDataNormalization() + "\" ");
        writer.println("                   type=\"" + network.getType() + "\" ");
        writer.println("                   coreRecipe=\"" + network.getCoreFunction().getCoreFunctionRecipe() + "\" ");
        writer.println("                   axonRecipe=\"" + network.getAxonFunction().getAxonFunctionRecipe() + "\" ");
        writer.println("                   weightRecipe=\"" + network.getWeightFunctionRecipe() + "\">");

        for(NeuralLayerInterface layer : network.getLayers()) {
            writer.println("        <layer description=\"" + layer.getDescription() + "\"");
            writer.println("               isInputLayer=\"" + layer.isInputLayer() + "\">");
            for(NeuronInterface neuron : layer.getNeurons()) {
                writer.println("            <neuron description=\"" + neuron.getDescription() + "\">");
                for(WeightFunction wweight : neuron.cloneWeights()) {
                    writer.println("                <weight>");
                    for(Double factor : wweight.getFactors()) {
                        writer.println("                    <factor>" + factor.toString() + "</factor>");
                    }
                    writer.println("                </weight>");
                }
                writer.println("            </neuron>");
            }
            writer.println("        </layer>");
        }
        writer.println("    </neuralNetwork>");
        writer.close();
    }

    /**
     * Parses neural network from file to new instance
     * @param filepath where is the file
     * @return new instance of parsed neural network
     * @throws Exception if is problem with file, or parsing
     */
    public NeuralNetworkInterface parse(String filepath) throws Exception {
        NeuralNetworkInterface network = new GeneuralNeuralNetwork();
        if(!filepath.contains(".xml"))
            filepath += ".xml";
        File file = new File(filepath);

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document document = dBuilder.parse(file);
        document.normalize();

        Node networkNode = document.getElementsByTagName("neuralNetwork").item(0);
        Element networkElement = (Element) networkNode;

        network.setDescription(networkElement.getAttribute("description"));
        Boolean biasPresence = Boolean.parseBoolean(networkElement.getAttribute("biasPresence"));
        network.setBiasPresence(biasPresence);
        network.setDataNormalization(DataNormalizationType.valueOf(networkElement.getAttribute("dataNormalization")));
        network.setType(NeuralNetworkType.valueOf(networkElement.getAttribute("type")));


        CoreFunctionRecipe coreFunctionRecipe = CoreFunctionRecipe
                .valueOf(networkElement.getAttribute("coreRecipe"));
        AxonFunctionRecipe axonFunctionRecipe = AxonFunctionRecipe
                .valueOf(networkElement.getAttribute("axonRecipe"));
        WeightFunctionRecipe weightFunctionRecipe = WeightFunctionRecipe
                .valueOf(networkElement.getAttribute("weightRecipe"));

        CoreFunction core = CoreFunctions.newInstance(coreFunctionRecipe);
        AxonFunction axon = AxonFunctions.newInstance(axonFunctionRecipe);

        NodeList layerNodeList = networkElement.getElementsByTagName("layer");

        List<NeuralLayerInterface> layers = new ArrayList<>();

        for(int layerNumber = 0; layerNumber < layerNodeList.getLength(); layerNumber++) {
            Node layerNode = layerNodeList.item(layerNumber);
            Element layerElement = (Element) layerNode;
            NeuralLayerInterface layer = new GeneuralLayer();
            layer.setDescription(layerElement.getAttribute("description"));
            layer.setInputLayer(Boolean.parseBoolean(layerElement.getAttribute("isInputLayer")));
            List<NeuronInterface> neurons = new ArrayList<>();
            NodeList neuronNodeList = layerElement.getElementsByTagName("neuron");
            for(int neuronNumber = 0; neuronNumber < neuronNodeList.getLength(); neuronNumber++) {
                Node neuronNode = neuronNodeList.item(neuronNumber);
                Element neuronElement = (Element) neuronNode;
                NodeList weightNodeList = neuronElement.getElementsByTagName("weight");
                List<WeightFunction> weights = new ArrayList<>();
                for(int weightNumber = 0; weightNumber < weightNodeList.getLength(); weightNumber++) {
                    Node weightNode = weightNodeList.item(weightNumber);
                    Element weightElement = (Element) weightNode;
                    WeightFunction weight = WeightFunctions.newInstance(weightFunctionRecipe, false);
                    NodeList factorNodeList = weightElement.getElementsByTagName("factor");
                    List<Double> factors = new ArrayList<>();
                    for(int factorNumber = 0; factorNumber < factorNodeList.getLength(); factorNumber++) {
                        Node factorNode = factorNodeList.item(factorNumber);
                        Element factorElement = (Element) factorNode;
                        factors.add(Double.parseDouble(factorElement.getTextContent()));
                    }
                    weight.setFactors(factors);
                    weight.setWeightFunctionRecipe(weightFunctionRecipe);
                    weights.add(weight);
                }
                NeuronInterface neuron = new GeneuralNeuron(weights, core, axon, biasPresence);
                neuron.setDescription(neuronElement.getAttribute("description"));
                neurons.add(neuron);
            }
            layer.setNeurons(neurons);
            layers.add(layer);
        }
        network.setLayers(layers);
        return network;
    }

    @Override
    public String getSenderName() {
        return "PARSER";
    }
}
