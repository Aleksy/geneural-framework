package aleksy.geneuralframework.common.logic;

/**
 * Interface for classes who can be logged in the console
 */
public interface Loggable {
    /**
     * Getter for constant name of sender
     * @return log sender name
     */
    String getSenderName();
}
