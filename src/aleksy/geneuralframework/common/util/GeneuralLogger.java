package aleksy.geneuralframework.common.util;

import aleksy.geneuralframework.common.constants.GeneuralBasicConstants;
import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;

import java.time.LocalDateTime;

/**
 * Geneural Framework message logger
 */
public class GeneuralLogger {
    /**
     * Method logs message in the console
     * @param message to log
     * @param sender of the message
     */
    public static void log(String message, String sender) {
            System.out.println(prefix(sender) + message);
    }

    /**
     * Method logs an exception in the console
     * @param e exception to print
     */
    public static void log(Exception e) {
            System.out.println(prefix("ERROR") + "An exception handled!");
            e.printStackTrace();
    }

    /**
     * Method logs a neural network details in the console
     * @param neuralNetwork to log
     */
    public static void log(NeuralNetworkInterface neuralNetwork) {
        System.out.println(prefix("NETWORK") + "Neural network");
        System.out.println(blankPrefix() + "    Basic data:");
        System.out.println(blankPrefix() + "        :description: " + neuralNetwork.getDescription());
        System.out.println(blankPrefix() + "        :type: " + neuralNetwork.getType());
        System.out.println(blankPrefix() + "        :core function recipe: "
                + neuralNetwork.getCoreFunction().getCoreFunctionRecipe());
        System.out.println(blankPrefix() + "        :axon function recipe: "
                + neuralNetwork.getAxonFunction().getAxonFunctionRecipe());
        System.out.println(blankPrefix() + "        :weight function recipe: "
                + neuralNetwork.getWeightFunctionRecipe());
        System.out.println(blankPrefix() + "        :is bias presented: " + neuralNetwork.isBiasPresence());
        System.out.println(blankPrefix() + "        :is data normalization active: " + neuralNetwork.getDataNormalization());
        System.out.println(blankPrefix() + "    Structure:");
        for(int i = 0; i < neuralNetwork.getLayers().size(); i++) {
            NeuralLayerInterface layer = neuralNetwork.getLayers().get(i);
            if(layer.isInputLayer())
                System.out.println(blankPrefix() + "        :input layer[" + i + "]:");
            else
                System.out.println(blankPrefix() + "        :layer[" + i + "]:");
            System.out.println(blankPrefix() + "            :description: " + layer.getDescription());
            System.out.println(blankPrefix() + "            :neurons: " + layer.getNeurons().size());
            System.out.println(blankPrefix() + "            :number of inputs: " + layer.numberOfInputs());
        }
    }

    public static void logo() {
        System.out.println("---------------------------------------------------------");
        System.out.println("| " + GeneuralBasicConstants.NAME + " " + GeneuralBasicConstants.VERSION + " " + GeneuralBasicConstants.AUTHOR + " |");
        System.out.println("---------------------------------------------------------");
        System.out.println();
    }

    private static String prefix(String sender) {
        StringBuilder prefix = new StringBuilder();
        prefix.append(LocalDateTime.now() + " geneural-framework [" + sender + "]");
        for(int i = prefix.length(); i < 70; i++)
            prefix.append(" ");
        return prefix.toString();
    }

    private static String blankPrefix() {
        StringBuilder prefix = new StringBuilder();
        for(int i = 0; i < 70; i++)
            prefix.append(" ");
        return prefix.toString();
    }
}
