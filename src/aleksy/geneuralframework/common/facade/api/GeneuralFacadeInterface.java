package aleksy.geneuralframework.common.facade.api;

import aleksy.geneuralframework.neuralnetstructure.builder.GeneuralNetworkBuilder;
import aleksy.geneuralframework.neuralnetstructure.builder.config.GeneuralNetworkStructureConfigurationsBuilder;
import aleksy.geneuralframework.neuralnetstructure.config.StructureConfigurations;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.teaching.builder.DeepLearningConfigurationsBuilder;
import aleksy.geneuralframework.teaching.builder.GeneticAlgorithmConfigurationsBuilder;
import aleksy.geneuralframework.teaching.common.api.TeachingFacadeInterface;
import aleksy.geneuralframework.teachingdata.builder.TeachingDataBuilder;

import java.io.IOException;

/**
 * Main framework facade
 */
public interface GeneuralFacadeInterface {
    /**
     * Getter of framework version
     * @return string representation of version
     */
    String getVersion();

    /**
     * Getter for framework name
     * @return framework name
     */
    String getFrameworkName();

    /**
     * Getter for author name, surname and city
     * @return author data
     */
    String getAuthor();

    /**
     * Getter for structure configurations builder
     * @return new instance of structure configurations builder, which can be used to create
     * new configurations for network builder
     */
    GeneuralNetworkStructureConfigurationsBuilder getStructureConfigurationsBuilder();

    /**
     * Getter for neural network builder
     * @param structureConfigurations configurations for new builder instance
     * @return builder, which can be used to building new neural network
     */
    GeneuralNetworkBuilder getNeuralNetworkBuilder(StructureConfigurations structureConfigurations);

    /**
     * Getter for teaching data builder
     * @return builder for teaching data
     */
    TeachingDataBuilder getTeachingDataBuilder();

    /**
     * Getter for new instance of genetic algorithm configurations builder
     * @return builder
     */
    GeneticAlgorithmConfigurationsBuilder getGeneticAlgorithmConfigurationsBuilder();

    /**
     * Starts a teaching of network
     * @return facade of teaching component, which has a methods of different teaching type execution
     */
    TeachingFacadeInterface startTeaching();

    /**
     * Allows Geneural Framework to logging messages to console
     * @param messageLogging permission to logging messages
     */
    void setMessageLogging(boolean messageLogging);

    /**
     * Getter for message logging boolean value
     * @return message logging
     */
    boolean isMessageLogging();

    /**
     * Getter for new instance of deep learning configurations builder
     * @return builder
     */
    DeepLearningConfigurationsBuilder getDeepLearningConfigurationsBuilder();

    /**
     * Logs a string message in the console
     * @param message to log
     * @param sender name of message sender
     */
    void log(String message, String sender);

    /**
     * Logs an exception
     * @param exception to log
     */
    void log(Exception exception);

    /**
     * Logs an information about neural network
     * @param neuralNetwork to log
     */
    void log(NeuralNetworkInterface neuralNetwork);

    /**
     * Saves a neural network to xml file
     * @param network to save
     * @param filepath where new xml file should be save
     * @throws IOException if is problem with file
     */
    void save(NeuralNetworkInterface network, String filepath) throws IOException;

    /**
     * Parses neural network from file to new instance
     * @param filepath where is the file
     * @return new instance of parsed neural network
     * @throws Exception if is problem with file, or parsing
     */
    NeuralNetworkInterface parse(String filepath) throws Exception;
}
