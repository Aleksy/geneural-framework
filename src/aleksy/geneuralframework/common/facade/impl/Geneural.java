package aleksy.geneuralframework.common.facade.impl;

import aleksy.geneuralframework.common.constants.GeneuralBasicConstants;
import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.logic.Loggable;
import aleksy.geneuralframework.common.util.GeneuralLogger;
import aleksy.geneuralframework.neuralnetstructure.builder.GeneuralNetworkBuilder;
import aleksy.geneuralframework.neuralnetstructure.builder.config.GeneuralNetworkStructureConfigurationsBuilder;
import aleksy.geneuralframework.neuralnetstructure.config.StructureConfigurations;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.parsing.GeneuralParser;
import aleksy.geneuralframework.teaching.builder.DeepLearningConfigurationsBuilder;
import aleksy.geneuralframework.teaching.builder.GeneticAlgorithmConfigurationsBuilder;
import aleksy.geneuralframework.teaching.common.api.TeachingFacadeInterface;
import aleksy.geneuralframework.teaching.common.impl.Teaching;
import aleksy.geneuralframework.teachingdata.builder.TeachingDataBuilder;

import java.io.IOException;

public class Geneural implements GeneuralFacadeInterface, Loggable {
    private boolean messageLogging;
    private GeneuralParser parser;

    public Geneural() {
        parser = new GeneuralParser();
        messageLogging = false;
    }

    @Override
    public String getVersion() {
        return GeneuralBasicConstants.VERSION;
    }
    @Override
    public String getFrameworkName() {
        return GeneuralBasicConstants.NAME;
    }

    @Override
    public String getAuthor() {
        return GeneuralBasicConstants.AUTHOR;
    }

    @Override
    public GeneuralNetworkBuilder getNeuralNetworkBuilder(StructureConfigurations structureConfigurations) {
        return new GeneuralNetworkBuilder(structureConfigurations, this);
    }

    @Override
    public GeneuralNetworkStructureConfigurationsBuilder getStructureConfigurationsBuilder() {
        return new GeneuralNetworkStructureConfigurationsBuilder(this);
    }

    @Override
    public TeachingDataBuilder getTeachingDataBuilder() {
        return new TeachingDataBuilder(this);
    }

    @Override
    public GeneticAlgorithmConfigurationsBuilder getGeneticAlgorithmConfigurationsBuilder() {
        return new GeneticAlgorithmConfigurationsBuilder();
    }

    @Override
    public TeachingFacadeInterface startTeaching() {
        return new Teaching(this);
    }

    @Override
    public void setMessageLogging(boolean messageLogging) {
        this.messageLogging = messageLogging;
        GeneuralLogger.logo();
        GeneuralLogger.log("Message logging: true", getSenderName());
    }

    @Override
    public boolean isMessageLogging() {
        return messageLogging;
    }

    @Override
    public DeepLearningConfigurationsBuilder getDeepLearningConfigurationsBuilder() {
        return new DeepLearningConfigurationsBuilder();
    }

    public static GeneuralFacadeInterface newInstance() {
        return new Geneural();
    }

    @Override
    public void log(Exception exception) {
        if(isMessageLogging())
            GeneuralLogger.log(exception);
    }

    @Override
    public void log(String message, String sender) {
        if(isMessageLogging())
            GeneuralLogger.log(message, sender);
    }

    @Override
    public void log(NeuralNetworkInterface neuralNetwork) {
        if(isMessageLogging())
            GeneuralLogger.log(neuralNetwork);
    }

    @Override
    public void save(NeuralNetworkInterface network, String filepath) throws IOException {
        parser.save(network, filepath);
    }

    @Override
    public NeuralNetworkInterface parse(String filepath) throws Exception {
        return parser.parse(filepath);
    }

    @Override
    public String getSenderName() {
        return "FACADE";
    }
}
