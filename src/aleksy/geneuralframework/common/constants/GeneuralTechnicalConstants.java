package aleksy.geneuralframework.common.constants;

/**
 * Technical constants values
 */
public class GeneuralTechnicalConstants {
    /**
     * Bias default value for neurons used in framework
     */
    public static final Double BIAS = 1.0;
}
