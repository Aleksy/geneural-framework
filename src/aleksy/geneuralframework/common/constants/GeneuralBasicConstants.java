package aleksy.geneuralframework.common.constants;

/**
 * Basic constants values from Geneural Framework
 */
public class GeneuralBasicConstants {
    /**
     * Geneural Framework Version
     */
    public static final String VERSION = "2.0.1-alpha";
    /**
     * Framework name
     */
    public static final String NAME = "Geneural Framework";
    /**
     * Framework author
     */
    public static final String AUTHOR = "Aleksy Bernat, Wroclaw";
}
