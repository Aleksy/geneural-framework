package aleksy.geneuralframework.teachingdata.builder;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.logic.Loggable;
import aleksy.geneuralframework.teachingdata.model.TeachingData;
import aleksy.geneuralframework.teachingdata.model.TeachingDataPackage;

import java.util.ArrayList;
import java.util.List;

/**
 * Teaching data builderó
 */
public class TeachingDataBuilder implements Loggable {
    private TeachingData teachingData;
    private GeneuralFacadeInterface geneural;

    /**
     * Constructor
     * @param geneural facade
     */
    public TeachingDataBuilder(GeneuralFacadeInterface geneural) {
        teachingData = new TeachingData();
        teachingData.setDataPackages(new ArrayList<>());
        this.geneural = geneural;
    }

    /**
     * Adds new data package to teaching data object
     * @param ins input vector for neural network
     * @param expectedOuts expected output vector from neural network
     * @return builder
     */
    public TeachingDataBuilder addDataPackage(List<Double> ins, List<Double> expectedOuts) {
        TeachingDataPackage dataPackage = new TeachingDataPackage();
        dataPackage.ins = ins;
        dataPackage.expectedOuts = expectedOuts;
        teachingData.getDataPackages().add(dataPackage);
        return this;
    }

    /**
     * Creates new instance of teaching data object
     * @return new instance of teaching data object
     */
    public TeachingData create() {
        geneural.log("Teaching data with " + teachingData.getDataPackages().size()
                + " packages was created.", getSenderName());
        return teachingData;
    }

    @Override
    public String getSenderName() {
        return "TEACHING-DATA-BLD";
    }
}
