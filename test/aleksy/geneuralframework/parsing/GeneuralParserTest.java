package aleksy.geneuralframework.parsing;

import aleksy.geneuralframework.neuralnetstructure.enumerate.*;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GeneuralParserTest {
    private GeneuralParser uut;

    @Before
    public void init() {
        uut = new GeneuralParser();
    }

    @Test
    public void shouldParseNetworkFromFile() throws Exception {
        // given
        String description = "neural network created in builder";
        DataNormalizationType dataNormalization=DataNormalizationType.SCALING_TO_AVERAGE;
        NeuralNetworkType type=NeuralNetworkType.FEEDFORWARD;
        CoreFunctionRecipe coreRecipe=CoreFunctionRecipe.DEFAULT_ADDER;
        AxonFunctionRecipe axonRecipe=AxonFunctionRecipe.TANH;
        WeightFunctionRecipe weightRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
        int numberOfLayers = 1;
        int numberOfNeurons = 1;
        int numberOfWeightsOfNeuron = 2;
        Double weight = 1.0;
        Double biasWeight = -0.1;

        // when
        NeuralNetworkInterface networkFromFile = uut.parse("test\\resources\\parsertest\\savedNetwork.xml");
        NeuronInterface neuron = networkFromFile
                .getLayers().get(0)
                .getNeurons().get(0);

        // then
        assertTrue(networkFromFile.isBiasPresence());
        assertEquals(description, networkFromFile.getDescription());
        assertEquals(dataNormalization, networkFromFile.getDataNormalization());
        assertEquals(type, networkFromFile.getType());
        assertEquals(coreRecipe, networkFromFile.getCoreFunction().getCoreFunctionRecipe());
        assertEquals(axonRecipe, networkFromFile.getAxonFunction().getAxonFunctionRecipe());
        assertEquals(weightRecipe, networkFromFile.getWeightFunctionRecipe());
        assertEquals(numberOfLayers, networkFromFile.numberOfLayers());
        assertEquals(numberOfNeurons, networkFromFile.size());
        assertEquals(numberOfWeightsOfNeuron, neuron.size());
        assertEquals(weight, neuron.getWeights().get(0).getFactors().get(0));
        assertEquals(biasWeight, neuron.getWeights().get(1).getFactors().get(0));
    }
}
