package aleksy.geneuralframework.neuralstructure.builder;

import static org.junit.Assert.*;

import aleksy.geneuralframework.neuralnetstructure.builder.EndpointBuilder;
import aleksy.geneuralframework.neuralnetstructure.enumerate.AxonFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.enumerate.CoreFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.enumerate.NeuralNetworkType;
import aleksy.geneuralframework.neuralnetstructure.enumerate.WeightFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.model.function.AxonFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.CoreFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.WeightFunction;
import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;
import aleksy.geneuralframework.neuralnetstructure.model.layer.impl.GeneuralLayer;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.neuralnetstructure.model.network.impl.GeneuralNeuralNetwork;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.impl.GeneuralNeuron;
import aleksy.geneuralframework.neuralnetstructure.util.AxonFunctions;
import aleksy.geneuralframework.neuralnetstructure.util.CoreFunctions;
import aleksy.geneuralframework.neuralnetstructure.util.WeightFunctions;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class EndpointBuilderTest {

    private EndpointBuilder uut;

    @Test
    public void shouldCreateNetworkWithDefaultDescriptionAndNotAddNewWeightsWhenBiasIsNotSet() {
        // given
        uut = new EndpointBuilder(createSimpleNetwork(false));
        String expectedDescription = "neural network created in builder";
        NeuralNetworkType expectedType = NeuralNetworkType.FEEDFORWARD;
        int expectedNumberOfWeights = 1;
        int actualNumberOfWeights;
        // when
        NeuralNetworkInterface actualNeuralNetwork = uut.create();
        actualNumberOfWeights = actualNeuralNetwork.getLayers().get(0).getNeurons().get(0).getWeights().size();
        // then
        assertEquals(expectedDescription, actualNeuralNetwork.getDescription());
        assertEquals(expectedType, actualNeuralNetwork.getType());
        assertEquals(expectedNumberOfWeights, actualNumberOfWeights);
    }

    @Test
    public void shouldCreateNetworkWithDefaultDescriptionAndAddNewWeightWhenBiasIsSet() {
        // given
        uut = new EndpointBuilder(createSimpleNetwork(true));
        String expectedDescription = "neural network created in builder";
        NeuralNetworkType expectedType = NeuralNetworkType.FEEDFORWARD;
        int expectedNumberOfWeights = 2;
        int actualNumberOfWeights;
        // when
        NeuralNetworkInterface actualNeuralNetwork = uut.create();
        actualNumberOfWeights = actualNeuralNetwork.getLayers().get(0).getNeurons().get(0).getWeights().size();
        // then
        assertEquals(expectedDescription, actualNeuralNetwork.getDescription());
        assertEquals(expectedType, actualNeuralNetwork.getType());
        assertEquals(expectedNumberOfWeights, actualNumberOfWeights);
    }

    @Test
    public void shouldCreateNetworkWithCustomDescriptionAndNotAddNewWeightsWhenBiasIsNotSet() {
        // given
        uut = new EndpointBuilder(createSimpleNetwork(false));
        String expectedDescription = "custom description";
        NeuralNetworkType expectedType = NeuralNetworkType.FEEDFORWARD;
        int expectedNumberOfWeights = 1;
        int actualNumberOfWeights;
        // when
        NeuralNetworkInterface actualNeuralNetwork = uut.create(expectedDescription);
        actualNumberOfWeights = actualNeuralNetwork.getLayers().get(0).getNeurons().get(0).getWeights().size();
        // then
        assertEquals(expectedDescription, actualNeuralNetwork.getDescription());
        assertEquals(expectedType, actualNeuralNetwork.getType());
        assertEquals(expectedNumberOfWeights, actualNumberOfWeights);
    }

    @Test
    public void shouldCreateNetworkWithCustomDescriptionAndAddNewWeightWhenBiasIsSet() {
        // given
        uut = new EndpointBuilder(createSimpleNetwork(true));
        String expectedDescription = "custom description";
        NeuralNetworkType expectedType = NeuralNetworkType.FEEDFORWARD;
        int expectedNumberOfWeights = 2;
        int actualNumberOfWeights;
        // when
        NeuralNetworkInterface actualNeuralNetwork = uut.create(expectedDescription);
        actualNumberOfWeights = actualNeuralNetwork.getLayers().get(0).getNeurons().get(0).getWeights().size();
        // then
        assertEquals(expectedDescription, actualNeuralNetwork.getDescription());
        assertEquals(expectedType, actualNeuralNetwork.getType());
        assertEquals(expectedNumberOfWeights, actualNumberOfWeights);
    }

    private NeuralNetworkInterface createSimpleNetwork(boolean biasPresence) {
        NeuralNetworkInterface nn = new GeneuralNeuralNetwork();
        NeuralLayerInterface layer = new GeneuralLayer();
        List<NeuralLayerInterface> layers = new ArrayList<>();
        WeightFunction weight = WeightFunctions.newInstance(WeightFunctionRecipe.DEFAULT_LINEAR, false);
        List<WeightFunction> weights = new ArrayList<>();
        weights.add(weight);
        CoreFunction core = CoreFunctions.newInstance(CoreFunctionRecipe.DEFAULT_ADDER);
        AxonFunction axon = AxonFunctions.newInstance(AxonFunctionRecipe.SINH);
        NeuronInterface neuron = new GeneuralNeuron(weights, core, axon, biasPresence);
        List<NeuronInterface> neurons = new ArrayList<>();
        neurons.add(neuron);
        layer.setNeurons(neurons);
        layers.add(layer);
        nn.setLayers(layers);
        nn.setBiasPresence(biasPresence);
        return nn;
    }
}
