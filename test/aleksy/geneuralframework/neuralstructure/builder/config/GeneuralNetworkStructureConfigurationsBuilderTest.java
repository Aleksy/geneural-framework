package aleksy.geneuralframework.neuralstructure.builder.config;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.facade.impl.Geneural;
import aleksy.geneuralframework.neuralnetstructure.builder.config.GeneuralNetworkStructureConfigurationsBuilder;
import aleksy.geneuralframework.neuralnetstructure.config.StructureConfigurations;
import aleksy.geneuralframework.neuralnetstructure.enumerate.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GeneuralNetworkStructureConfigurationsBuilderTest {
    GeneuralNetworkStructureConfigurationsBuilder uut;

    @Before
    public void init() {
        GeneuralFacadeInterface facade = Geneural.newInstance();
        uut = facade.getStructureConfigurationsBuilder();
    }

    @Test
    public void shouldCreateCustomConfigurations() {
        // given
        StructureConfigurations expectedSc = createTestConfigurations();
        StructureConfigurations actualSc;

        // when
        uut.setWeightFunction(expectedSc.weightFunctionRecipe);
        uut.setDataNormalization(expectedSc.dataNormalization);
        uut.setAxonFunction(expectedSc.axonFunctionRecipe);
        uut.setNeuralNetworkType(expectedSc.neuralNetworkType);
        uut.setBiasPresence(expectedSc.biasPresence);
        uut.setCoreFunction(expectedSc.coreFunctionRecipe);
        actualSc = uut.create();

        // then
        assertEquals(expectedSc.weightFunctionRecipe, actualSc.weightFunctionRecipe);
        assertEquals(expectedSc.axonFunctionRecipe, actualSc.axonFunctionRecipe);
        assertEquals(expectedSc.coreFunctionRecipe, actualSc.coreFunctionRecipe);
        assertEquals(expectedSc.biasPresence, actualSc.biasPresence);
        assertEquals(expectedSc.dataNormalization, actualSc.dataNormalization);
        assertEquals(expectedSc.neuralNetworkType, actualSc.neuralNetworkType);
    }

    @Test
    public void shouldCreateDefaultConfigurations() {
        // given
        StructureConfigurations defaultSc = new StructureConfigurations();
        defaultSc.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
        defaultSc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
        defaultSc.axonFunctionRecipe = AxonFunctionRecipe.TANH;
        defaultSc.neuralNetworkType = NeuralNetworkType.FEEDFORWARD;
        defaultSc.biasPresence = true;
        defaultSc.dataNormalization = DataNormalizationType.SCALING_TO_AVERAGE;
        StructureConfigurations actualSc;


        // when
        actualSc = uut.defaultConfigurations().create();

        // then
        assertEquals(defaultSc.weightFunctionRecipe, actualSc.weightFunctionRecipe);
        assertEquals(defaultSc.axonFunctionRecipe, actualSc.axonFunctionRecipe);
        assertEquals(defaultSc.coreFunctionRecipe, actualSc.coreFunctionRecipe);
        assertEquals(defaultSc.biasPresence, actualSc.biasPresence);
        assertEquals(defaultSc.dataNormalization, actualSc.dataNormalization);
        assertEquals(defaultSc.neuralNetworkType, actualSc.neuralNetworkType);
    }

    private StructureConfigurations createTestConfigurations() {
        StructureConfigurations sc = new StructureConfigurations();
        sc.dataNormalization = DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR;
        sc.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
        sc.neuralNetworkType = NeuralNetworkType.FEEDFORWARD;
        sc.biasPresence = true;
        sc.axonFunctionRecipe = AxonFunctionRecipe.SINH;
        sc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
        return sc;
    }
}
