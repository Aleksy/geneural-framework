package aleksy.geneuralframework.neuralstructure.builder;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.facade.impl.Geneural;
import aleksy.geneuralframework.neuralnetstructure.builder.NetworkWithInputLayerBuilder;
import aleksy.geneuralframework.neuralnetstructure.config.StructureConfigurations;
import aleksy.geneuralframework.neuralnetstructure.enumerate.*;
import aleksy.geneuralframework.neuralnetstructure.model.function.AxonFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.CoreFunction;
import aleksy.geneuralframework.neuralnetstructure.model.function.WeightFunction;
import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;
import aleksy.geneuralframework.neuralnetstructure.model.layer.impl.GeneuralLayer;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.neuralnetstructure.model.network.impl.GeneuralNeuralNetwork;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.impl.GeneuralNeuron;
import aleksy.geneuralframework.neuralnetstructure.util.WeightFunctions;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NetworkWithInputLayerBuilderTest {

    private NetworkWithInputLayerBuilder uut;

    private CoreFunction core;
    private AxonFunction axon;

    @Before
    public void init() {
        GeneuralFacadeInterface geneural = Geneural.newInstance();
        uut = new NetworkWithInputLayerBuilder(createTestConfigurations(), createSimpleNetwork(), core, axon, geneural);
    }

    @Test
    public void shouldAddLayerToNetwork() {
        // given
        int expectedNumberOfNeuronsInLayer = 3;
        int expectedNumberOfLayers = 2;
        String expectedDescription = "layer created in builder";

        // when
        uut.addLayer(expectedNumberOfNeuronsInLayer);
        NeuralNetworkInterface actualNeuralNetwork = uut.endBuildingNetwork(false).create();
        int actualNumberOfNeurons = actualNeuralNetwork.getLayers().get(1).size();
        int actualNumberOfLayers = actualNeuralNetwork.numberOfLayers();

        // then
        assertEquals(expectedNumberOfNeuronsInLayer, actualNumberOfNeurons);
        assertEquals(expectedNumberOfLayers, actualNumberOfLayers);
        assertEquals(expectedDescription, actualNeuralNetwork.getLayers().get(1).getDescription());
    }

    @Test
    public void shouldAddLayerToNetworkWithCustomDescription() {
        // given
        int expectedNumberOfNeuronsInLayer = 3;
        int expectedNumberOfLayers = 2;
        String expectedDescription = "custom description";

        // when
        uut.addLayer(expectedNumberOfNeuronsInLayer, expectedDescription);
        NeuralNetworkInterface actualNeuralNetwork = uut.endBuildingNetwork(false).create();
        int actualNumberOfNeurons = actualNeuralNetwork.getLayers().get(1).size();
        int actualNumberOfLayers = actualNeuralNetwork.numberOfLayers();

        // then
        assertEquals(expectedNumberOfNeuronsInLayer, actualNumberOfNeurons);
        assertEquals(expectedNumberOfLayers, actualNumberOfLayers);
        assertEquals(expectedDescription, actualNeuralNetwork.getLayers().get(1).getDescription());
    }

    private NeuralNetworkInterface createSimpleNetwork() {
        NeuralNetworkInterface nn = new GeneuralNeuralNetwork();
        WeightFunction weight = WeightFunctions.newInstance(WeightFunctionRecipe.DEFAULT_LINEAR, false);
        List<WeightFunction> weights = new ArrayList<>();
        weights.add(weight);
        nn.setBiasPresence(false);
        List<NeuralLayerInterface> layers = new ArrayList<>();
        layers.add(createSimpleInputLayer());
        nn.setLayers(layers);
        nn.setDataNormalization(DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR);
        return nn;
    }

    private StructureConfigurations createTestConfigurations() {
        StructureConfigurations sc = new StructureConfigurations();
        sc.dataNormalization = DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR;
        sc.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
        sc.neuralNetworkType = NeuralNetworkType.FEEDFORWARD;
        sc.biasPresence = true;
        sc.axonFunctionRecipe = AxonFunctionRecipe.SINH;
        sc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
        return sc;
    }

    private NeuralLayerInterface createSimpleInputLayer() {
        NeuralLayerInterface layer = new GeneuralLayer();
        WeightFunction weight = WeightFunctions.newInstance(WeightFunctionRecipe.DEFAULT_LINEAR, false);
        List<WeightFunction> weights = new ArrayList<>();
        weights.add(weight);
        NeuronInterface neuron = new GeneuralNeuron(weights, core, axon, false);
        List<NeuronInterface> neurons = new ArrayList<>();
        neurons.add(neuron);
        layer.setNeurons(neurons);
        return layer;
    }
}
