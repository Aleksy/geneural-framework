package aleksy.geneuralframework.neuralstructure.builder;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.facade.impl.Geneural;
import aleksy.geneuralframework.neuralnetstructure.builder.EndpointBuilder;
import aleksy.geneuralframework.neuralnetstructure.builder.GeneuralNetworkBuilder;
import aleksy.geneuralframework.neuralnetstructure.builder.NetworkWithInputLayerBuilder;
import aleksy.geneuralframework.neuralnetstructure.config.StructureConfigurations;
import aleksy.geneuralframework.neuralnetstructure.enumerate.*;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class GeneuralNetworkBuilderTest {

    private GeneuralNetworkBuilder uut;
    GeneuralFacadeInterface geneural;

    @Before
    public void init() {
        geneural = Geneural.newInstance();
        uut = geneural.getNeuralNetworkBuilder(createTestConfigurations());
    }

    @Test
    public void shouldReturnNetworkWithInputLayerBuilder() {
        // when
        NetworkWithInputLayerBuilder actualBuilder = uut.addDefaultInputLayer(1, 2);

        // then
        assertEquals(NetworkWithInputLayerBuilder.class, actualBuilder.getClass());
    }

    @Test
    public void shouldReturnEndpointBuilder() {
        // when
        EndpointBuilder actualBuilder = uut.buildSingleLayerNetwork(1, 2, false);

        // then
        assertEquals(EndpointBuilder.class, actualBuilder.getClass());
    }

    @Test
    public void shouldCorrectlyCreateNeuralNetwork() {
        // given
        String description = "neural network created in builder";
        DataNormalizationType dataNormalization=DataNormalizationType.SCALING_TO_AVERAGE;
        NeuralNetworkType type=NeuralNetworkType.FEEDFORWARD;
        CoreFunctionRecipe coreRecipe=CoreFunctionRecipe.DEFAULT_ADDER;
        AxonFunctionRecipe axonRecipe=AxonFunctionRecipe.TANH;
        WeightFunctionRecipe weightRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
        int numberOfLayers = 1;
        int numberOfNeurons = 1;
        int numberOfWeightsOfNeuron = 1;
        Double weight = 1.0;

        // when
        NeuralNetworkInterface actualNetwork = geneural.getNeuralNetworkBuilder(
                geneural.getStructureConfigurationsBuilder()
                    .defaultConfigurations()
                        .setBiasPresence(false)
                    .create()
        ).addDefaultInputLayer(1, 1)
                .endBuildingNetwork(false)
                .create();
        NeuronInterface neuron = actualNetwork
                .getLayers().get(0)
                .getNeurons().get(0);

        // then
        assertFalse(actualNetwork.isBiasPresence());
        assertEquals(description, actualNetwork.getDescription());
        assertEquals(dataNormalization, actualNetwork.getDataNormalization());
        assertEquals(type, actualNetwork.getType());
        assertEquals(coreRecipe, actualNetwork.getCoreFunction().getCoreFunctionRecipe());
        assertEquals(axonRecipe, actualNetwork.getAxonFunction().getAxonFunctionRecipe());
        assertEquals(weightRecipe, actualNetwork.getWeightFunctionRecipe());
        assertEquals(numberOfLayers, actualNetwork.numberOfLayers());
        assertEquals(numberOfNeurons, actualNetwork.size());
        assertEquals(numberOfWeightsOfNeuron, neuron.size());
        assertEquals(weight, neuron.getWeights().get(0).getFactors().get(0));
    }

    private StructureConfigurations createTestConfigurations() {
        StructureConfigurations sc = new StructureConfigurations();
        sc.dataNormalization = DataNormalizationType.NORMALIZATION_TO_UNIT_VECTOR;
        sc.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
        sc.neuralNetworkType = NeuralNetworkType.FEEDFORWARD;
        sc.biasPresence = true;
        sc.axonFunctionRecipe = AxonFunctionRecipe.SINH;
        sc.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
        return sc;
    }
}
