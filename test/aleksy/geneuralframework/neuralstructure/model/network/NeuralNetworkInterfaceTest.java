package aleksy.geneuralframework.neuralstructure.model.network;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.facade.impl.Geneural;
import aleksy.geneuralframework.neuralnetstructure.enumerate.AxonFunctionRecipe;
import aleksy.geneuralframework.neuralnetstructure.enumerate.DataNormalizationType;
import aleksy.geneuralframework.neuralnetstructure.exception.IncorrectNumberOfInputsInLayerException;
import aleksy.geneuralframework.neuralnetstructure.exception.IncorrectNumberOfInputsInNeuronException;
import aleksy.geneuralframework.neuralnetstructure.model.layer.NeuralLayerInterface;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.neuralnetstructure.model.neuron.NeuronInterface;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class NeuralNetworkInterfaceTest {
    private NeuralNetworkInterface uut;
    private final int neuronsInInputLayer = 2;
    private final int sizeOfInputVector = 1;
    private final int neuronsInOutputLayer = 1;


    @Before
    public void init() {
        uut = createNeuralNetwork();
    }

    @Test
    public void shouldReturnInputNeuralLayer() {
        // when
        NeuralLayerInterface returnedLayer = uut.getInputLayer();

        // then
        assertEquals(neuronsInInputLayer, returnedLayer.size());
    }

    @Test
    public void shouldReturnOutputNeuralLayer() {
        // when
        NeuralLayerInterface returnedLayer = uut.getOutputLayer();

        // then
        assertEquals(neuronsInOutputLayer, returnedLayer.size());
    }

    @Test
    public void shouldReturnCorrectDescribingValues() {
        // given
        int expectedAmountOfNeurons = 3;
        int expectedAmountOfLayers = 2;

        // when
        int actualAmountOfNeurons = uut.size();
        int actualAmountOfLayers = uut.numberOfLayers();

        // then
        assertEquals(expectedAmountOfLayers, actualAmountOfLayers);
        assertEquals(expectedAmountOfNeurons, actualAmountOfNeurons);
    }

    @Test
    public void shouldProduceNewInstanceOfNetworkWithTheSameParameters() {
        // given
        NeuralNetworkInterface expectedNetwork = createNeuralNetwork();

        //when
        NeuralNetworkInterface actualNetwork = uut.copy();

        //then
        assertEquals(expectedNetwork.getLayers().size(), actualNetwork.getLayers().size());
        assertEquals(expectedNetwork.getDescription(), actualNetwork.getDescription());
        assertEquals(expectedNetwork.getType(), actualNetwork.getType());
        assertEquals(expectedNetwork.isBiasPresence(), actualNetwork.isBiasPresence());
        assertEquals(expectedNetwork.getAxonFunction().getAxonFunctionRecipe(),
                actualNetwork.getAxonFunction().getAxonFunctionRecipe());
        assertEquals(expectedNetwork.getWeightFunctionRecipe(), actualNetwork.getWeightFunctionRecipe());
        assertEquals(expectedNetwork.getCoreFunction().getCoreFunctionRecipe(),
                actualNetwork.getCoreFunction().getCoreFunctionRecipe());
        assertEquals(expectedNetwork.getDataNormalization(), actualNetwork.getDataNormalization());
        assertEquals(expectedNetwork.getInputLayer().size(), actualNetwork.getInputLayer().size());
        assertEquals(expectedNetwork.getOutputLayer().size(), actualNetwork.getOutputLayer().size());
        assertEquals(expectedNetwork.getInputLayer().getDescription(), actualNetwork.getInputLayer().getDescription());
        assertEquals(expectedNetwork.getOutputLayer().getDescription(), actualNetwork.getOutputLayer().getDescription());
    }

    @Test
    public void shouldCreateBiasWeights() {
        // given
        int expectedNumberOfWeights = 7;

        // when
        uut.createBiasWeights();
        List<NeuronInterface> neurons = new ArrayList<>();
        for(NeuralLayerInterface layer : uut.getLayers())
            neurons.addAll(layer.getNeurons());
        int actualNumberOfWeights = 0;
        for(NeuronInterface neuron : neurons)
            actualNumberOfWeights += neuron.size();

        // then
        assertEquals(expectedNumberOfWeights, actualNumberOfWeights);
    }

    @Test
    public void shouldReturnCorrectValueOnOutput() throws IncorrectNumberOfInputsInLayerException, IncorrectNumberOfInputsInNeuronException {
        // given
        double value = 1.2;
        List<Double> inputVector = new ArrayList<>();
        inputVector.add(value);
        double expectedValueOnOutput = 2*value;

        // when
        List<Double> actualOutput = uut.f(inputVector);

        // then
        assertTrue(actualOutput.get(0).equals(expectedValueOnOutput));
    }

    private NeuralNetworkInterface createNeuralNetwork() {
        GeneuralFacadeInterface g = Geneural.newInstance();
        return g.getNeuralNetworkBuilder(
                g.getStructureConfigurationsBuilder()
                    .defaultConfigurations()
                        .setDataNormalization(DataNormalizationType.NO_NORMALIZATION)
                        .setAxonFunction(AxonFunctionRecipe.LINEAR)
                        .setBiasPresence(false)
                    .create()
        ).addDefaultInputLayer(neuronsInInputLayer, sizeOfInputVector)
                .addLayer(neuronsInOutputLayer)
                .endBuildingNetwork(false)
                .create();
    }
}
