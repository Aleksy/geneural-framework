package aleksy.geneuralframework.teachingdata.builder;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.facade.impl.Geneural;
import aleksy.geneuralframework.teachingdata.model.TeachingData;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TeachingDataBuilderTest {
    private TeachingDataBuilder uut;

    @Before
    public void init() {
        GeneuralFacadeInterface geneural = Geneural.newInstance();
        uut = new TeachingDataBuilder(geneural);
    }

    @Test
    public void shouldCreateTeachingDataWithTwoPackages() {
        // given
        List<Double> firstPackageInputs = new ArrayList<>(Arrays.asList(0.1, 0.2, 0.3));
        List<Double> firstPackageOutputs = new ArrayList<>(Arrays.asList(0.9));
        List<Double> secondPackageInputs = new ArrayList<>(Arrays.asList(0.3, 0.4, 0.5));
        List<Double> secondPackageOutputs = new ArrayList<>(Arrays.asList(0.3));
        int amountOfPacages = 2;

        // when
        uut.addDataPackage(firstPackageInputs, firstPackageOutputs);
        uut.addDataPackage(secondPackageInputs, secondPackageOutputs);
        TeachingData actualTd = uut.create();

        //then
        assertEquals(amountOfPacages, actualTd.getDataPackages().size());
        assertEquals((Double)0.9, actualTd.getDataPackages().get(0).expectedOuts.get(0));
    }
}
