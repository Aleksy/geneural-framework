package aleksy.geneuralframework.common.facade;

import aleksy.geneuralframework.common.constants.GeneuralBasicConstants;
import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.facade.impl.Geneural;
import aleksy.geneuralframework.neuralnetstructure.builder.GeneuralNetworkBuilder;
import aleksy.geneuralframework.neuralnetstructure.builder.config.GeneuralNetworkStructureConfigurationsBuilder;
import aleksy.geneuralframework.neuralnetstructure.config.StructureConfigurations;
import aleksy.geneuralframework.neuralnetstructure.enumerate.*;
import aleksy.geneuralframework.teaching.builder.DeepLearningConfigurationsBuilder;
import aleksy.geneuralframework.teaching.builder.GeneticAlgorithmConfigurationsBuilder;
import aleksy.geneuralframework.teaching.common.api.TeachingFacadeInterface;
import aleksy.geneuralframework.teachingdata.builder.TeachingDataBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GeneuralTest {
    private GeneuralFacadeInterface uut;

    @Before
    public void init() {
        uut = Geneural.newInstance();
    }

    @Test
    public void shouldReturnTheCorrectBasicDataAboutFramework() {
        // given
        String expectedVersion = GeneuralBasicConstants.VERSION;
        String expectedAuthor = GeneuralBasicConstants.AUTHOR;
        String expectedName = GeneuralBasicConstants.NAME;

        // when
        String actualVersion = uut.getVersion();
        String actualAuthor = uut.getAuthor();
        String actualName = uut.getFrameworkName();

        // then
        assertEquals(expectedAuthor, actualAuthor);
        assertEquals(expectedName, actualName);
        assertEquals(expectedVersion, actualVersion);
    }

    @Test
    public void shouldReturnGeneuralBuilder() {
        // given
        StructureConfigurations config = prepareTestConfigurations();

        // when
        GeneuralNetworkBuilder builder = uut.getNeuralNetworkBuilder(config);

        // then
        assertNotNull(builder);
    }

    @Test
    public void shouldReturnGeneuralStructureConfigurationsBuilder() {
        // when
        GeneuralNetworkStructureConfigurationsBuilder builder = uut.getStructureConfigurationsBuilder();

        // then
        assertNotNull(builder);
    }

    @Test
    public void shouldReturnTeachingDataBuilder() {
        // when
        TeachingDataBuilder builder = uut.getTeachingDataBuilder();

        // then
        assertNotNull(builder);
    }

    @Test
    public void shouldReturnGeneticAlgorithmConfigurationsBuilder() {
        // when
        GeneticAlgorithmConfigurationsBuilder builder = uut.getGeneticAlgorithmConfigurationsBuilder();

        // then
        assertNotNull(builder);
    }

    @Test
    public void shouldReturnDeepLearningConfigurationsBuilder() {
        // when
        DeepLearningConfigurationsBuilder builder = uut.getDeepLearningConfigurationsBuilder();

        // then
        assertNotNull(builder);
    }

    @Test
    public void shouldReturnTeachingFacade() {
        // when
        TeachingFacadeInterface facade = uut.startTeaching();

        // then
        assertNotNull(facade);
    }

    @Test
    public void shouldSetMessageLogging() {
        // given
        boolean messageLoggingBeforeMethodCall = uut.isMessageLogging();

        // when
        uut.setMessageLogging(!messageLoggingBeforeMethodCall);

        // then
        assertEquals(!messageLoggingBeforeMethodCall, uut.isMessageLogging());
    }

    private StructureConfigurations prepareTestConfigurations() {
        StructureConfigurations config = new StructureConfigurations();
        config.biasPresence = false;
        config.neuralNetworkType = NeuralNetworkType.FEEDFORWARD;
        config.axonFunctionRecipe = AxonFunctionRecipe.SINH;
        config.coreFunctionRecipe = CoreFunctionRecipe.DEFAULT_ADDER;
        config.weightFunctionRecipe = WeightFunctionRecipe.DEFAULT_LINEAR;
        config.dataNormalization = DataNormalizationType.NO_NORMALIZATION;
        return config;
    }
}
