package aleksy.geneuralframework.teaching.builder;

import aleksy.geneuralframework.teaching.config.DeepLearningConfigurations;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DeepLearningConfigurationsBuilderTest {
    private final int ITEARTIONS = 1000;
    private final Double LEARNING_RATE = 0.1;

    private DeepLearningConfigurationsBuilder uut;

    @Before
    public void init() {
        uut = new DeepLearningConfigurationsBuilder();
    }

    @Test
    public void shouldCreateCustomConfigurations() {
        // given
        DeepLearningConfigurations expectedDlc = createTestConfig();

        // when
        uut.setIterations(expectedDlc.iterations);
        uut.setLearningRate(expectedDlc.learningRate);
        DeepLearningConfigurations actualDlc = uut.create();

        // then
        assertEquals(ITEARTIONS, actualDlc.iterations);
        assertEquals(LEARNING_RATE, actualDlc.learningRate);
    }

    private DeepLearningConfigurations createTestConfig() {
        DeepLearningConfigurations dlc = new DeepLearningConfigurations();
        dlc.iterations = ITEARTIONS;
        dlc.learningRate = LEARNING_RATE;
        return dlc;
    }
}
