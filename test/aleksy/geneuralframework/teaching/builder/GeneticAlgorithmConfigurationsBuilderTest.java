package aleksy.geneuralframework.teaching.builder;

import aleksy.geneuralframework.teaching.config.GeneticAlgorithmConfigurations;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GeneticAlgorithmConfigurationsBuilderTest {
    private final int AMOUNT_OF_NETWORKS = 100;
    private final Double MUTATION_CHANCE = 0.01;
    private final int GENERATIONS = 1000;
    private final Double PERCENTAGE_AMOUNT_OF_HYBRIDIZED_NETWORKS = 0.4;
    private final int EXPECTED_AMOUNT_OF_NETWORKS_ON_OUTPUT = 10;

    private GeneticAlgorithmConfigurationsBuilder uut;

    @Before
    public void init() {
        uut = new GeneticAlgorithmConfigurationsBuilder();
    }

    @Test
    public void shouldCreateCustomConfigurations() {
        // given
        GeneticAlgorithmConfigurations gac = createTestConfig();

        // when
        uut.setPercentageAmountOfHybridizedNetworks(gac.percentageAmountOfHybridizedNetworks);
        uut.setMutationChance(gac.mutationChance);
        uut.setExpectedAmountOfNetworksOnOutput(gac.expectedAmountOfNetworksOnOutput);
        uut.setAmountOfNetworksInPopulation(gac.amountOfNetworks);
        uut.setAmountOfGenerations(gac.generations);
        GeneticAlgorithmConfigurations actualGac = uut.create();

        // then
        assertEquals(PERCENTAGE_AMOUNT_OF_HYBRIDIZED_NETWORKS, (Double)actualGac.percentageAmountOfHybridizedNetworks);
        assertEquals(AMOUNT_OF_NETWORKS, actualGac.amountOfNetworks);
        assertEquals(MUTATION_CHANCE, (Double)actualGac.mutationChance);
        assertEquals(EXPECTED_AMOUNT_OF_NETWORKS_ON_OUTPUT, actualGac.expectedAmountOfNetworksOnOutput);
        assertEquals(GENERATIONS, actualGac.generations);
    }

    private GeneticAlgorithmConfigurations createTestConfig() {
        GeneticAlgorithmConfigurations gac = new GeneticAlgorithmConfigurations();
        gac.generations = GENERATIONS;
        gac.percentageAmountOfHybridizedNetworks = PERCENTAGE_AMOUNT_OF_HYBRIDIZED_NETWORKS;
        gac.mutationChance = MUTATION_CHANCE;
        gac.expectedAmountOfNetworksOnOutput = EXPECTED_AMOUNT_OF_NETWORKS_ON_OUTPUT;
        gac.amountOfNetworks = AMOUNT_OF_NETWORKS;
        return gac;
    }
}
