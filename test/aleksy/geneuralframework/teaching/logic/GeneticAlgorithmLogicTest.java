package aleksy.geneuralframework.teaching.logic;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.facade.impl.Geneural;
import aleksy.geneuralframework.neuralnetstructure.enumerate.DataNormalizationType;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.teaching.config.GeneticAlgorithmConfigurations;
import aleksy.geneuralframework.teaching.exception.GeneticAlgorithmException;
import aleksy.geneuralframework.teachingdata.model.TeachingData;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class GeneticAlgorithmLogicTest {
    private GeneticAlgorithmLogic uut;
    private GeneuralFacadeInterface geneural;

    @Before
    public void init() {
        geneural = Geneural.newInstance();
        uut = new GeneticAlgorithmLogic(geneural);
    }

    @Test
    public void shouldThrowExceptionWhenMutationChanceIsIncorrect() {
        // given
        boolean exceptionThrown = false;
        GeneticAlgorithmConfigurations gac;
        gac = prepareTestConfig();
        gac.mutationChance = 3;

        // when
        try {
            uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), gac);
        } catch (GeneticAlgorithmException e) {
            exceptionThrown = true;
        }

        // then
        assertTrue(exceptionThrown);
    }

    @Test
    public void shouldThrowExceptionWhenGenerationsAreIncorrect() {
        // given
        boolean exceptionThrown = false;
        GeneticAlgorithmConfigurations gac;
        gac = prepareTestConfig();
        gac.generations = -10;

        // when
        try {
            uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), gac);
        } catch (GeneticAlgorithmException e) {
            exceptionThrown = true;
        }

        // then
        assertTrue(exceptionThrown);
    }

    @Test
    public void shouldThrowExceptionWhenExpectedNetworksOnOutputIncorrect() {
        // given
        boolean exceptionThrown = false;
        GeneticAlgorithmConfigurations gac;
        gac = prepareTestConfig();
        gac.expectedAmountOfNetworksOnOutput = -1;

        // when
        try {
            uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), gac);
        } catch (GeneticAlgorithmException e) {
            exceptionThrown = true;
        }

        // then
        assertTrue(exceptionThrown);
    }

    @Test
    public void shouldThrowExceptionWhenAmountOfNetworkIsIncorrect() {
        // given
        boolean exceptionThrown = false;
        GeneticAlgorithmConfigurations gac;
        gac = prepareTestConfig();
        gac.amountOfNetworks = -100;

        // when
        try {
            uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), gac);
        } catch (GeneticAlgorithmException e) {
            exceptionThrown = true;
        }

        // then
        assertTrue(exceptionThrown);
    }

    @Test
    public void shouldThrowExceptionWhenPercentageAmountOfHybridizedNetworksIncorrect() {
        // given
        boolean exceptionThrown = false;
        GeneticAlgorithmConfigurations gac;
        gac = prepareTestConfig();
        gac.percentageAmountOfHybridizedNetworks = 42;

        // when
        try {
            uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), gac);
        } catch (GeneticAlgorithmException e) {
            exceptionThrown = true;
        }

        // then
        assertTrue(exceptionThrown);
    }

    private GeneticAlgorithmConfigurations prepareTestConfig() {
        return geneural.getGeneticAlgorithmConfigurationsBuilder()
                    .setAmountOfGenerations(1000)
                    .setAmountOfNetworksInPopulation(100)
                    .setExpectedAmountOfNetworksOnOutput(10)
                    .setPercentageAmountOfHybridizedNetworks(0.4)
                    .setMutationChance(0.01)
                .create();
    }

    private NeuralNetworkInterface prepareTestNeuralNetwork() {
        return geneural.getNeuralNetworkBuilder(
                geneural.getStructureConfigurationsBuilder()
                        .defaultConfigurations()
                        .setBiasPresence(false)
                        .setDataNormalization(DataNormalizationType.NO_NORMALIZATION)
                        .create()
        ).addDefaultInputLayer(1, 1)
                .endBuildingNetwork(false)
                .create();
    }

    private TeachingData prepareTestTeachingData() {
        List<Double> inputs = new ArrayList<>(Collections.singletonList(1.0));
        List<Double> outputs = new ArrayList<>(Collections.singletonList(0.0));
        return geneural.getTeachingDataBuilder()
                .addDataPackage(inputs, outputs)
                .create();
    }
}
