package aleksy.geneuralframework.teaching.logic;

import aleksy.geneuralframework.common.facade.api.GeneuralFacadeInterface;
import aleksy.geneuralframework.common.facade.impl.Geneural;
import aleksy.geneuralframework.neuralnetstructure.enumerate.DataNormalizationType;
import aleksy.geneuralframework.neuralnetstructure.exception.abstr.NeuralNetworkException;
import aleksy.geneuralframework.neuralnetstructure.model.network.NeuralNetworkInterface;
import aleksy.geneuralframework.teaching.config.DeepLearningConfigurations;
import aleksy.geneuralframework.teaching.exception.DeepLearningConfigurationsException;
import aleksy.geneuralframework.teachingdata.model.TeachingData;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DeepLearningLogicTest {
    private DeepLearningLogic uut;
    private GeneuralFacadeInterface geneural;

    @Before
    public void init() {
        geneural = Geneural.newInstance();
        uut = new DeepLearningLogic(geneural);
    }

    @Test(expected = DeepLearningConfigurationsException.class)
    public void shouldThrowExceptionWhenIterationsAreLessThanZero() throws DeepLearningConfigurationsException, NeuralNetworkException {
        // given
        DeepLearningConfigurations config = prepareTestConfig();
        config.iterations = -1;

        // when
        uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), config);
        // then exception
    }

    @Test(expected = DeepLearningConfigurationsException.class)
    public void shouldThrowExceptionWhenLearningRateAreInvalid() throws DeepLearningConfigurationsException, NeuralNetworkException {
        // given
        DeepLearningConfigurations config = prepareTestConfig();
        config.learningRate = -0.2;

        // when
        uut.start(prepareTestTeachingData(), prepareTestNeuralNetwork(), config);
        // then exception
    }

    private TeachingData prepareTestTeachingData() {
        List<Double> inputs = new ArrayList<>(Collections.singletonList(1.0));
        List<Double> outputs = new ArrayList<>(Collections.singletonList(0.0));
        return geneural.getTeachingDataBuilder()
                .addDataPackage(inputs, outputs)
                .create();
    }

    private DeepLearningConfigurations prepareTestConfig() {
        return geneural.getDeepLearningConfigurationsBuilder()
                .setLearningRate(1.0)
                .setIterations(1).create();
    }

    private NeuralNetworkInterface prepareTestNeuralNetwork() {
        return geneural.getNeuralNetworkBuilder(
                geneural.getStructureConfigurationsBuilder()
                    .defaultConfigurations()
                    .setBiasPresence(false)
                    .setDataNormalization(DataNormalizationType.NO_NORMALIZATION)
                    .create()
        ).addDefaultInputLayer(1, 1)
                .endBuildingNetwork(false)
                .create();
    }
}
