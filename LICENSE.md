# License

[Open source](https://opensource.org/osd)

# Some tips for user

1. If you want to use Geneural Framework as experimantal environment just do it.
   Repository is cloneable, so you can download it, or get by "git clone". It is
   possible to include code as external library of your project.
2. If you are using Geneural Framework I would to know that, so please let me
   know about it. I also would to be in your "Credits" :)
3. I really don't like a plagiarism, if you want to use Geneural Framework in
   illegal way let me know first. We'll think together.

# For developers

1. If you are developer and you want to explore and study my code you can do it.
   I accept external ideas, so if you have a proposition how to make Geneural
   Framework better - let me know :)
2. I also accept your code modifications - if you have a specific case to use
   modified Geneural Framework and you want to share this code, make your source
   free for download and use.
3. Please remember that I want to be co-author of your work.

# Summary (tl;dr)

1. Don't use my code in illegal way.
2. Let me know if you use it and what do you want to do with GF.
3. Place my name somewhere, I would to be considered as co-author.