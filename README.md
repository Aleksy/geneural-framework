## Geneural Framework

**What GF is?**

Geneural is a framework to create neural networks and teach them in
easy way. GF supports:  
- building multilayer feedforward neural networks  
- teaching neural networks by genetic algorithm and standard deep learning algorithm  
- Saving neural network to xml file and parsing networks from file.  
  

To become familiar with GF workflow and usage please see a [Wiki](https://gitlab.com/Aleksy/geneural-framework/wikis/home).
In the [Changelog](https://gitlab.com/Aleksy/geneural-framework/blob/master/CHANGELOG.md) you can find informations about history of changes, older versions.
Please also take a moment to read the [License](https://gitlab.com/Aleksy/geneural-framework/blob/master/LICENSE.md).  
  
**Download**  
  
Newest version:  
[Geneural Framework 2.0.1-alpha](https://drive.google.com/file/d/1reyp9D5AloXyvrcLYdHwU0_2RJlP6xQE/view?usp=sharing)  

For older versions please visit [Changelog](https://gitlab.com/Aleksy/geneural-framework/blob/master/CHANGELOG.md).  
  
Instalation:  
If you want to use Geneural Framework as external library:  
    1. Read the [License](https://gitlab.com/Aleksy/geneural-framework/blob/master/LICENSE.md)  
    2. Click the download link above.  
    3. Include JAR file to your project as external library.
  
If you want to explore and study source code:
    1. Read the [License](https://gitlab.com/Aleksy/geneural-framework/blob/master/LICENSE.md)  
    2. Clone repository ('git clone https://gitlab.com/Aleksy/geneural-framework')  
  
Enjoy!  
  
**Future**  
  
In future I want to implement more types of neural network, like Kohonen's, or  
Hopfield's networks.