# Changelog
  
**[2.0.1-alpha](https://drive.google.com/file/d/1reyp9D5AloXyvrcLYdHwU0_2RJlP6xQE/view?usp=sharing)** *11.05.18 "Big test update"*  
  
*Added*  
  
- lot's of tests to make code more qualitative  
  
*Changed*  
  
- Not implemented networks types are not available now  
  
*Fixed*  
  
- error in deep learning, first layer weights were updated two times  
- building single layer network by one method prevented randomizing the weights  
- cleaning stuff  
  
Code quality info:  
Tested classes: 80%  
Tested methods: 65%  
Line coverage: 52%  
  
**[2.0.0-alpha](https://drive.google.com/file/d/1Ykibx9PpIoSaSxnsYBtaSbQ5Zck1HlSQ/view?usp=sharing)** *26.04.18 "First documented update"*  
  
  *Added*  
- General documentation (java docs)  
- Data validation in deep learning algorithm  
- Data normalization / scaling types  
- Time measurement in algorithms  
  
  *Changed*  
- Now message logging permission is assigned to the GeneuralFacadeInterface instance.  
- Deep learning algorithm shows more console data  
- Setting message logging to true shows basic informations about Geneural Framework.  
  
  *Fixed*  
- Neuron shows informations about difference between input vector size and expected
input vector size when exception is thrown.  
  
**1.0.0-alpha**  
- basic api defined  
- internal documentation and wiki added  
- logo presentation in logger added  
- last bugs from prealpha fixed  
- from now I try to keep semantic versioning :)  
  
**0.2.0-prealpha**  
- global message logging configuration added  
  
**0.1.1-prealpha**  
- parser added  
- deep learning algorithm added  
  
**0.1.0-prealpha**  
- genetic algorithm added  
  
**0.0.1-prealpha**  
- initialization of basic components  
- basic buildings of networks  
